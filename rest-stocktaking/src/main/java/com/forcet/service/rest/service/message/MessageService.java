package com.forcet.service.rest.service.message;

import com.forcet.service.rest.model.notification.mail.Email;
import com.forcet.service.rest.model.notification.phone.Phone;
import com.forcet.service.rest.model.response.Summary;

public interface MessageService {

    Summary<?> sendMessageEmail(Email email);

    Summary<?> sendMessagePhone(Phone phone);

}
