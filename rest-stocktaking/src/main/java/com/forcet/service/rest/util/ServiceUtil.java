package com.forcet.service.rest.util;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

import static java.time.temporal.ChronoUnit.*;

@Slf4j
public class ServiceUtil {

    public BigDecimal getSubtractDecimal(BigDecimal mayor, BigDecimal less) {
        return mayor.subtract(less);
    }

    public BigDecimal getAddDecimal(BigDecimal mayor, BigDecimal less) {
        return mayor.add(less);
    }

    public long getDaysCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {
        LocalDate comparativeDate = dateAfter !=  null ? dateAfter : LocalDate.now();
        return DAYS.between(dateBefore, comparativeDate);
    }

    public long getMonthsCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {
        LocalDate comparativeDate = dateAfter !=  null ? dateAfter : LocalDate.now();
        return MONTHS.between(dateBefore, comparativeDate);
    }

    public long getYearsCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {
        LocalDate comparativeDate = dateAfter !=  null ? dateAfter : LocalDate.now();
        return YEARS.between(dateBefore, comparativeDate);
    }

    public BigDecimal getBigDecimal( Object value ) {
        BigDecimal ret = null;
        if( value != null ) {
            if( value instanceof BigDecimal ) {
                ret = (BigDecimal) value;
            } else if( value instanceof String ) {
                ret = new BigDecimal( (String) value );
            } else if( value instanceof BigInteger) {
                ret = new BigDecimal( (BigInteger) value );
            } else if( value instanceof Number ) {
                ret = BigDecimal.valueOf(((Number) value).doubleValue());
            }
        }
        return ret;
    }

    public LocalDate getLocalDate(String date) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            return LocalDate.parse(date, formatter);

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), String.format("Puede que no sea una fecha %s: ", e.getMessage()));
            return LocalDate.now();
        }
    }

    public String getStringDate(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        try {
            return formatter.format(localDate);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), String.format("Erro al formatear %s: ", e.getMessage()));
            return formatter.format(LocalDate.now());
        }
    }

    public boolean containsDates(LocalDate contain) {
        LocalDate now = LocalDate.now();
        LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth());
        LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfMonth());
        return (!contain.isBefore( firstDay)) && (contain.isBefore(lastDay));
    }

    public boolean containsDateRanges(LocalDate contain, LocalDate max, LocalDate min) {
        return (!contain.isBefore(min)) && (contain.isBefore(max));
    }




}
