package com.forcet.service.rest.repository.user;

import com.forcet.service.rest.model.user.response.UserResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends MongoRepository<UserResponse, String> {

}
