package com.forcet.service.rest.service.fixed;


import com.forcet.service.rest.model.fixed.request.FixedRequest;
import com.forcet.service.rest.model.fixed.response.FixedResponse;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FixedService {

    ResponseEntity<Summary<FixedResponse>> saveFixed(FixedRequest request);

    ResponseEntity<Summary<FixedResponse>> updateFixed(FixedRequest request);

    ResponseEntity<Summary<FixedResponse>> deleteFixed(String fixedId);

    ResponseEntity<Summary<FixedResponse>> deleteAllFixed(String code);

    ResponseEntity<Summary<FixedResponse>> findFixedById(String fixedId);

    ResponseEntity<Summary<FixedResponse>> findAllFixedByUser(String userId);

    ResponseEntity<Summary<List<String>>> saveFixedFile(MultipartFile[] files, String userId);


}
