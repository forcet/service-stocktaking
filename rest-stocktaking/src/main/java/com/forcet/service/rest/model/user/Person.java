package com.forcet.service.rest.model.user;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    @ApiModelProperty(position = 2, notes = "${person.description.legalId}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String legalId;

    @ApiModelProperty(position = 3, notes = "${person.description.legalIdType}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String legalIdType;

    @ApiModelProperty(position = 4, notes = "${person.description.names}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String names;

    @ApiModelProperty(position = 5, notes = "${person.description.lastName}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String lastName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 6, notes = "person.description.birthDate", required = true, example = "yyyy/mm/dd")
    private LocalDate birthDate;

    @ApiModelProperty(position = 7, notes = "${person.description.email}",  required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String email;

    @ApiModelProperty(position = 8, notes = "${person.description.phone}",  required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String phone;


}
