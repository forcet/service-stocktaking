package com.forcet.service.rest.service.reports.template;

import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.report.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class VegetableReport extends FillReports {

    List<SpecieVegetableResponse> listField;

    public VegetableReport(List<SpecieVegetableResponse> listField) {
        this.listField = listField;
    }

    public static VegetableReport create(List<SpecieVegetableResponse> listField) {
        return new VegetableReport(listField);
    }

    @Override
    protected Field changeFieldReport() {
        List<SpecieVegetableResponse> responseList = this.listField;
        List<List<Object>> rowList = responseList.stream().map(response -> {
            List<Object> row = new ArrayList<>();
            row.add(response.getVegetableId());
            row.add(response.getName());
            row.add(response.getBuyDate());
            row.add(response.getCodeMaintenance());
            row.add(response.getCost());
            row.add(response.getTotal());
            row.add(response.getDiaryConsume());
            return row;
        }).collect(Collectors.toList());
        return Field.builder()
                .columnName(Arrays.asList("Código","Nombre", "Nacimiento", "Alimento", "Costo", "Ejemplares", "Consumo diario" ))
                .valueRows(rowList)
                .build() ;
    }
}
