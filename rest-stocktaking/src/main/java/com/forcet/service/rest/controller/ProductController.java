package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.product.request.ProductRequest;
import com.forcet.service.rest.model.product.response.ProductResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.product.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("products")
@Api(value = "Creaci\u00F3n de Productos", description = "API que permite el registro de Productos", tags = {"Productos"})
public class ProductController {

    @Autowired
    ProductService productService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveProduct", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${product.save.description}")
    public ResponseEntity<Summary<ProductResponse>> saveProduct(@RequestBody @Valid ProductRequest productRequest) {
        return productService.saveProduct(productRequest);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllProductsByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${product.findAll.description}")
    public ResponseEntity<Summary<ProductResponse>> findAllProductByUser(@RequestParam("userCode") String userCode) {
        return productService.findAllProductByUser(userCode);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteProduct")
    @ApiOperation(value = "${product.delelte.description}")
    public ResponseEntity<Summary<ProductResponse>> deleteProduct(@RequestParam String productId) {
        return productService.deleteProduct(productId);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteAllProduct")
    @ApiOperation(value = "${product.delelte.description}")
    public ResponseEntity<Summary<ProductResponse>> deleteAllProduct(@RequestParam String code) {
        return productService.deleteAllProduct(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateProduct")
    @ApiOperation(value = "${product.update.description}")
    public ResponseEntity<Summary<ProductResponse>> updateProduct(@RequestBody @Valid ProductRequest productRequest) {
        return  productService.updateProduct(productRequest);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/uploadProduct",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE })
    @ApiOperation(value = "${file.save.description}")
    public ResponseEntity<Summary<List<String>>> uploadProduct(@RequestParam @Valid MultipartFile[] files, @RequestParam String code) {
        return productService.saveFile(files, code);
    }

}
