package com.forcet.service.rest.service.reports.template;

import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.report.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AnimalReport extends FillReports{

    List<SpecieAnimalResponse> listField;

    public AnimalReport(List<SpecieAnimalResponse> listField) {
        this.listField = listField;
    }

    public static AnimalReport create(List<SpecieAnimalResponse> listField) {
        return new AnimalReport(listField);
    }

    @Override
    protected Field changeFieldReport() {
        List<SpecieAnimalResponse> responseList = this.listField;
        List<List<Object>> rowList = responseList.stream().map(response -> {
            List<Object> row = new ArrayList<>();
            row.add(response.getAnimalId());
            row.add(response.getName());
            row.add(response.getBirthDate());
            row.add(response.getCodeMaintenance());
            row.add(response.getCost());
            row.add(response.getTotal());
            row.add(response.getDiaryConsume());
            row.add(response.getAge());
            row.add(response.getMonth());
            row.add(response.getYear());
            return row;
        }).collect(Collectors.toList());
        return Field.builder()
                .columnName(Arrays.asList("Código","Nombre", "Nacimiento", "Alimento", "Costo", "Ejemplares", "Consumo diario", "Edad en días", "Edad en meses", "Edad en años" ))
                .valueRows(rowList)
                .build() ;
    }
}
