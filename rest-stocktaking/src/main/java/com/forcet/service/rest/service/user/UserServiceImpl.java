package com.forcet.service.rest.service.user;


import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.user.request.UserRequest;
import com.forcet.service.rest.model.user.response.UserResponse;
import com.forcet.service.rest.repository.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    @Autowired
    UserRepository userRepository;

    @Override
    public ResponseEntity<Summary<UserResponse>> saveUser(UserRequest userRequest) {
        Summary<UserResponse> summary = new Summary<>();
        ResponseEntity<Summary<UserResponse>> responseEntity;
        try {
            UserResponse userResponse = userRepository.findById(userRequest.getUsername()).orElse(null);
            if (userResponse == null) {
                UserResponse newBeforePasswordUser = getNewUserResponse(userRequest);
                newBeforePasswordUser.hashPassword();
                UserResponse newUser = userRepository.save(newBeforePasswordUser);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(newUser), HttpStatus.OK);
            } else {
                responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("El usuario existe"),
                        "No se puede ingresar el usuario, ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<UserResponse>> updateUser(UserRequest userRequest) {
        Summary<UserResponse> summary = new Summary<>();
        ResponseEntity<Summary<UserResponse>> responseEntity;
        try {
            UserResponse consultUserResponse = userRepository.findById(userRequest.getUsername()).orElse(null);
            if (consultUserResponse != null) {
                UserResponse updateUser = userRepository.save(getUpdateUserResponse(userRequest, consultUserResponse));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(updateUser), HttpStatus.OK);
            } else {
                responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun usuario"),
                        "No se ha encontra el usuario buscado", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<UserResponse>> deleteUser(String userId) {
        Summary<UserResponse> summary = new Summary<>();
        ResponseEntity<Summary<UserResponse>> responseEntity;
        try {
            UserResponse userResponse = userRepository.findById(userId).orElse(null);
            if (userResponse != null) {
                userRepository.delete(userResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            } else {
                responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun usuario") ,
                        "No se encontre el registro solicitado", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<UserResponse>> findById(String userName) {
        Summary<UserResponse> summary = new Summary<>();
        ResponseEntity<Summary<UserResponse>> responseEntity;
        try {
            UserResponse userResponse = userRepository.findById(userName).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(userResponse), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("Ocurrio un error interno"),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<UserResponse>> findAll() {
        Summary<UserResponse> summary = new Summary<>();
        ResponseEntity<Summary<UserResponse>> responseEntity;
        try {
            List<UserResponse> userResponseList = userRepository.findAll();
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(userResponseList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    private UserResponse getNewUserResponse(UserRequest request) {
        return UserResponse.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .rol(request.getRol())
                .status(request.getStatus())
                .person(request.getPerson())
                .creationDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();
    }

    private UserResponse getUpdateUserResponse(UserRequest request, UserResponse response) {
        return UserResponse.builder()
                .username(response.getUsername())
                .password(response.getPassword())
                .rol(request.getRol())
                .status(request.getStatus())
                .person(request.getPerson())
                .creationDate(response.getCreationDate())
                .updateDate(LocalDateTime.now())
                .build();
    }


}
