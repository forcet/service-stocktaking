package com.forcet.service.rest.scheduler.template;

import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;

import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.service.message.MessageService;

import java.math.BigDecimal;

public class TaskAnimal extends BuilderScheduler {

    SpecieAnimalResponse specieAnimalResponse;

    public TaskAnimal(MessageService messageService, SpecieAnimalResponse specieAnimalResponse, MaintenanceRepository maintenanceRepository) {
        super.messageService = messageService;
        super.maintenanceRepository = maintenanceRepository;
        this.specieAnimalResponse = specieAnimalResponse;
    }

    public static TaskAnimal create(MessageService messageService, SpecieAnimalResponse specieAnimalResponse,  MaintenanceRepository maintenanceRepository) {
        return new TaskAnimal(messageService, specieAnimalResponse, maintenanceRepository);
    }


    @Override
    protected BigDecimal getSubtractQuality(BigDecimal qualityActual) {
        BigDecimal totalConsume = specieAnimalResponse.getTotal().multiply(specieAnimalResponse.getDiaryConsume());
        return  qualityActual.subtract(totalConsume);
    }

    @Override
    protected String getSpecieName() {
        return specieAnimalResponse.getName();
    }
}
