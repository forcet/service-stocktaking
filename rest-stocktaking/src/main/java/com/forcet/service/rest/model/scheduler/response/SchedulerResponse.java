package com.forcet.service.rest.model.scheduler.response;


import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;


@Getter
@Builder
@Document(collection = "scheduler")
public class SchedulerResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${scheduler.description.id}")
    private String id;

    @ApiModelProperty(position = 2, notes = "${scheduler.description.userCode}")
    private String userCode;

    @ApiModelProperty(position = 3, notes = "${scheduler.description.productId}")
    private String productId;

    @ApiModelProperty(position = 4, notes = "${scheduler.description.productName}")
    private String productName;

    @ApiModelProperty(position = 5, notes = "${scheduler.description.nameScheduler}")
    private String nameScheduler;

    @ApiModelProperty(position = 6, notes = "${scheduler.description.personalizeMessage}")
    private String personalizeMessage;

    @ApiModelProperty(position = 7, notes = "${scheduler.description.startDate}")
    private LocalDate startDate;

    @ApiModelProperty(position = 8, notes = "${scheduler.description.endDate}")
    private LocalDate endDate;

    @ApiModelProperty(position = 9, notes = "${scheduler.description.notNotifyMail}")
    private String notNotifyMail;

    @ApiModelProperty(position = 10, notes = "${scheduler.description.notNotifyPhone}")
    private String notNotifyPhone;


}
