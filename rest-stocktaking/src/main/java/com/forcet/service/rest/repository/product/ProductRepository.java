package com.forcet.service.rest.repository.product;

import com.forcet.service.rest.model.product.response.ProductResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;


public interface ProductRepository extends MongoRepository<ProductResponse, String> {


    @Query("{'username' : ?0 }")
    List<ProductResponse> findAllProductByUser(String username);

    @Query("{'username' : ?0,  'productId' : ?1}")
    Optional<ProductResponse> findProductByUser(String username, String productId);

}
