package com.forcet.service.rest.model.report.chart;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class InfoChart {

    List<Data> data;
    List<String> colors;
}
