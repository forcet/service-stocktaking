package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.fixed.request.FixedRequest;
import com.forcet.service.rest.model.fixed.response.FixedResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.fixed.FixedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("fixed")
@Api(value = "Creaci\u00F3n de herramientas de trabajo", description = "API que permite el registro de herramientas de trabajo", tags = {"Herramientas"})
public class FixedController {

    @Autowired
    FixedService fixedService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveFixed", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${fixed.save.description}")
    public ResponseEntity<Summary<FixedResponse>> saveFixed(@RequestBody @Valid FixedRequest request) {
        return fixedService.saveFixed(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllFixedByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${fixed.findAll.description}")
    public ResponseEntity<Summary<FixedResponse>> findAllFixedByUser(@RequestParam("userCode") String userCode) {
        return fixedService.findAllFixedByUser(userCode);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteFixed")
    @ApiOperation(value = "${fixed.delelte.description}")
    public ResponseEntity<Summary<FixedResponse>> deleteFixed(@RequestParam String fixedId) {
        return fixedService.deleteFixed(fixedId);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteAllFixed")
    @ApiOperation(value = "${fixed.delelte.description}")
    public ResponseEntity<Summary<FixedResponse>> deleteAllFixed(@RequestParam String code) {
        return fixedService.deleteAllFixed(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateFixed")
    @ApiOperation(value = "${fixed.update.description}")
    public ResponseEntity<Summary<FixedResponse>> updateFixed(@RequestBody @Valid FixedRequest request) {
        return  fixedService.updateFixed(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/uploadFixed",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE })
    @ApiOperation(value = "${file.save.fixed.description}")
    public ResponseEntity<Summary<List<String>>> uploadFixed(@RequestParam @Valid MultipartFile[] files, @RequestParam String code) {
        return fixedService.saveFixedFile(files, code);
    }



}
