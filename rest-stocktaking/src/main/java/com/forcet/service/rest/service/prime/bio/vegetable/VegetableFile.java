package com.forcet.service.rest.service.prime.bio.vegetable;

import com.forcet.service.rest.model.prime.bio.vegetable.request.SpecieVegetableRequest;
import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.prime.bio.vegetable.SpecieVegetableRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.service.template.files.BuilderFilesData;
import com.forcet.service.rest.util.ServiceUtil;

import java.util.List;

public class VegetableFile extends BuilderFilesData {

    ServiceUtil serviceUtil = new ServiceUtil();

    SpecieVegetableRepository specieVegetableRepository;

    MaintenanceRepository maintenanceRepository;


    public VegetableFile(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, SpecieVegetableRepository specieVegetableRepository, MaintenanceRepository maintenanceRepository) {
        super.fileRepository = fileRepository;
        super.messageService = messageService;
        super.userRepository = userRepository;
        this.specieVegetableRepository = specieVegetableRepository;
        this.maintenanceRepository = maintenanceRepository;

    }

    public static VegetableFile create(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, SpecieVegetableRepository specieVegetableRepository, MaintenanceRepository maintenanceRepository) {
        return new VegetableFile(fileRepository, messageService, userRepository, specieVegetableRepository, maintenanceRepository);
    }

    @Override
    protected void validateProduct(String[] data, String code, List<String> errors) {
        String codeVegetable = data[0];
        SpecieVegetableResponse response = specieVegetableRepository.findSpecieVegetableByUser(code, codeVegetable).orElse(null);
        if (response == null) {
            SpecieVegetableRequest request = SpecieVegetableRequest
                    .builder()
                    .vegetableId(codeVegetable)
                    .userCode(code)
                    .name(data[1])
                    .buyDate(data[2])
                    .codeMaintenance(data[3])
                    .cost(serviceUtil.getBigDecimal(data[4]))
                    .total(serviceUtil.getBigDecimal(data[5]))
                    .diaryConsume(serviceUtil.getBigDecimal(data[6]))
                    .notifyUser(data[7])
                    .build();
            SpecieVegetableServiceImpl service = new SpecieVegetableServiceImpl();
            SpecieVegetableResponse spVegetableResponse = service.getNewSpecieVegetableResponse(request);
            spVegetableResponse.setCodeMaintenanceDescription(getMaintenanceDescription(request.getUserCode(), request.getCodeMaintenance()));
            specieVegetableRepository.save(spVegetableResponse);
        } else {
            errors.add(String.format("Elemento ya registrado previamente %s", data[0]));
        }
    }

    private String getMaintenanceDescription(String code, String codeMaintenance) {
        MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(code, codeMaintenance).orElse(null);
        if (maintenanceResponse != null) {
                return maintenanceResponse.getName();
        }
        return null;
    }

    @Override
    protected String getTransaction() {
        return "Especie Vegetal";
    }

    @Override
    protected int getSizeFile() {
        return 8;
    }
}
