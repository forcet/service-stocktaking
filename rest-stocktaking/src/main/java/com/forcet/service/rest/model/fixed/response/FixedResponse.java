package com.forcet.service.rest.model.fixed.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
@Document(collection = "fixed")
public class FixedResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${fixed.description.id}")
    String id;

    @ApiModelProperty(position = 2, notes = "${fixed.description.code}")
    String fixedId;  //Codigo

    @ApiModelProperty(position = 3, notes = "${fixed.description.userCode}")
    String userCode; //Codigo de usuario

    @ApiModelProperty(position = 4, notes = "${fixed.description.name}")
    String name; //Nombre

    @ApiModelProperty(position = 5, notes = "${fixed.description.quantities}")
    BigDecimal quantities; //Cantidad

    @ApiModelProperty(position = 6, notes = "${fixed.description.status}")
    String status; //Estado

    @ApiModelProperty(position = 7, notes = "${fixed.description.cost}")
    BigDecimal cost; //

    @ApiModelProperty(position = 8, notes = "${fixed.description.quantityLose}")
    BigDecimal quantityLose;

    @ApiModelProperty(position = 9, notes = "${fixed.description.messageLose}")
    List<String> messageLose;

}
