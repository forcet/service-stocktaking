package com.forcet.service.rest.controller;

import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.prime.maintenace.resquest.MaintenanceRequest;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.prime.maintenance.MaintenanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("maintenance")
@Api(value = "Creaci\u00F3n de Actualizar (no recuerdo)", description = "API que permite el registro de Actualizar (no recuerdo)", tags = {"Maintenance"})
public class MaintenanceController {

    @Autowired
    MaintenanceService maintenanceService;


    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveMaintenance", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${maintenance.save.description}")
    public ResponseEntity<Summary<MaintenanceResponse>> saveMaintenance(@RequestBody @Valid MaintenanceRequest request) {
        return maintenanceService.saveMaintenance(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllMaintenanceByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${maintenance.findAll.description}")
    public ResponseEntity<Summary<MaintenanceResponse>> findAllMaintenanceByUser(@RequestParam("userCode") String userCode) {
        return maintenanceService.findAllMaintenanceByUser(userCode);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteMaintenance")
    @ApiOperation(value = "${maintenance.delelte.description}")
    public ResponseEntity<Summary<MaintenanceResponse>> deleteMaintenance(@RequestParam String maintenanceId) {
        return maintenanceService.deleteMaintenance(maintenanceId);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteAllMaintenance")
    @ApiOperation(value = "${maintenance.delelte.description}")
    public ResponseEntity<Summary<MaintenanceResponse>> deleteAllMaintenance(@RequestParam String code) {
        return maintenanceService.deleteAllMaintenance(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateMaintenance")
    @ApiOperation(value = "${maintenance.update.description}")
    public ResponseEntity<Summary<MaintenanceResponse>> updateMaintenance(@RequestBody @Valid MaintenanceRequest request) {
        return  maintenanceService.updateMaintenance(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/uploadMaintenance",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE })
    @ApiOperation(value = "${file.save.maintenance.description}")
    public ResponseEntity<Summary<List<String>>> uploadMaintenance(@RequestParam @Valid MultipartFile[] files, @RequestParam String code) {
        return maintenanceService.saveMaintenanceFile(files, code);
    }
}
