package com.forcet.service.rest.service.template.files;

import com.forcet.service.rest.model.notification.mail.Email;
import com.forcet.service.rest.model.notification.phone.Phone;
import com.forcet.service.rest.model.user.Person;
import com.forcet.service.rest.model.user.response.UserResponse;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public abstract class BuilderFilesData {

    protected FileRepository fileRepository;

    protected MessageService messageService;

    protected UserRepository userRepository;


    public List<String> saveUserFiles(MultipartFile[] files, String userId) {
        fileRepository.deleteAll(userId);
        fileRepository.init(userId);
        Arrays.asList(files).forEach(file -> fileRepository.save(file, userId));
        List<String> names = Arrays.stream(files).map(MultipartFile::getOriginalFilename).collect(Collectors.toList());
        List<String> errors = new ArrayList<>();
        names.forEach(file -> {
            Resource load = fileRepository.load(file, userId);
            errors.addAll(getResultNewProduct(load, userId));
            fileRepository.deleteFile(file, userId);
        });
        sendNotification(userId, errors);
        return errors;
    }

    private List<String> getResultNewProduct(Resource load, String code) {
        List<String> registerWithErrors = new ArrayList<>();
        try {
            Stream<String> lines = Files.lines(Paths.get(load.getURI()));
            lines.forEach(line -> {
                log.info(String.format("line: %s", line));
                String[] product = line.split("\\|");
                if(product.length == getSizeFile()) {
                    validateProduct(product, code, registerWithErrors);
                } else {
                    registerWithErrors.add(String.format("No se tiene el numero total de campos en la linea : %s", line));
                }
            });
            lines.close();
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return registerWithErrors;
    }

    private void sendNotification(String userId, List<String> error) {
        UserResponse userRequest = userRepository.findById(userId).orElse(null);
        if (userRequest != null) {
            Person person = userRequest.getPerson();
            String errors = String.join("\n", error);
            String message = String.format("Estimado %s: Se ha generado una carga por archivo de %s, revisar correo para mas detalle",
                    person.getNames(), getTransaction());
            messageService.sendMessagePhone(Phone.builder()
                    .message(message)
                    .phoneNumber(person.getPhone())
                    .codeCountry("593")
                    .build());
            message = message.concat("\n").concat(errors);
            messageService.sendMessageEmail(Email.builder()
                    .body(message)
                    .affair(String.format("Carga de archivo: %s", getTransaction()))
                    .sender(person.getEmail())
                    .build());
        }
    }


    protected abstract void validateProduct(String[] data, String code, List<String> errors);

    protected abstract String getTransaction();

    protected abstract int getSizeFile();
}
