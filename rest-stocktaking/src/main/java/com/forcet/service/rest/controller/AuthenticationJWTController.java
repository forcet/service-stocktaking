package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.login.Login;
import com.forcet.service.rest.model.login.TokenResult;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.security.Constants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;

@RestController
@Api(value = "Autenticaci\u00F3n con Json Web Token", description = "API de Autenticaci\u00F3n, retorna un JWT que debe ser utilizado en el resto de transacciones", tags = {"Autenticaci\u00F3n"})
public class AuthenticationJWTController {

    @Value("${time.token}")
    private long expirationTime;

    @Value("${http.auth.token}")
    private String apiRequest;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping(value = "auth", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Summary<TokenResult>> authenticationToken(@RequestBody Login info) throws AuthenticationException {


        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        apiRequest,
                        info,
                        new ArrayList<>())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String authorities = "ROLE_".concat(info.getRol());

        String token = Jwts.builder()
                .setSubject((String) authentication.getPrincipal())
                .claim("user", info.getUsername())
                .claim("authorities", authorities)
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .signWith(SignatureAlgorithm.HS512, Constants.SECRET.getBytes(Charset.defaultCharset()))
                .compact();

        Summary<TokenResult> summary = new Summary<>();
        summary = summary.createSuccessResponse(TokenResult.builder().rol(info.getRol()).username(info.getUsername()).token(Constants.TOKEN_PREFIX + token).build());
        return new ResponseEntity<>(summary, HttpStatus.OK);

    }

}
