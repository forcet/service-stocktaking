package com.forcet.service.rest.model.prime.bio.animal.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "specieAnimal")
public class SpecieAnimalResponse {


    @Id
    String id;

    @ApiModelProperty(position = 1, notes = "${animal.description.code}", required = true)
    String animalId; //Codigo tabla

    @ApiModelProperty(position = 2, notes = "${animal.description.userCode}", required = true)
    String userCode; //Codigo Usuario

    @ApiModelProperty(position = 3, notes = "${animal.description.name}", required = true)
    String name; //Nombre especie

    @ApiModelProperty(position = 4, notes = "${animal.description.birthDate}", required = true)
    String birthDate; //Fecha nacimiento o compra

    @ApiModelProperty(position = 5, notes = "${animal.description.codeMaintenance}", required = true)
    String codeMaintenance; //Codigo del alimento - relacion Mantenanice

    @ApiModelProperty(position = 6, notes = "${animal.description.codeMaintenance}", required = true)
    String codeMaintenanceDescription; //Codigo del alimento - relacion Mantenanice

    @ApiModelProperty(position = 7, notes = "${animal.description.cost}", required = true)
    BigDecimal cost; //cuanto costo compralo

    @ApiModelProperty(position = 8, notes = "${animal.description.total}", required = true)
    BigDecimal total; // unidades si es necesario

    @ApiModelProperty(position = 9, notes = "${animal.description.diaryConsume}", required = true)
    BigDecimal diaryConsume; // Consumo de alimentacion a restar en

    @ApiModelProperty(position = 10, notes = "${animal.description.loseSpecie}")
    BigDecimal loseSpecie;

    @ApiModelProperty(position = 11, notes = "${animal.description.messageLose}")
    List<String> messageLose;

    @ApiModelProperty(position = 12, notes = "${animal.description.notifyUser}")
    String notifyUser;

    @ApiModelProperty(position = 13, notes = "${animal.description.age}")
    BigDecimal age; // Edad

    @ApiModelProperty(position = 14, notes = "${animal.description.age}")
    BigDecimal month; // Edad

    @ApiModelProperty(position = 15, notes = "${animal.description.age}")
    BigDecimal year; // Edad


}
