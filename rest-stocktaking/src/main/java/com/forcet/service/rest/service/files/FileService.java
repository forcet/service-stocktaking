package com.forcet.service.rest.service.files;

import com.forcet.service.rest.model.files.File;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface FileService {

    Summary<?> save(MultipartFile[] file, String code);

    Summary<Resource> load(String name, String code);

    Summary<?> deleteAll(String code);

    Summary<List<File>> loadAll(String code);

    Summary<?> deleteFile(String name, String code);

}
