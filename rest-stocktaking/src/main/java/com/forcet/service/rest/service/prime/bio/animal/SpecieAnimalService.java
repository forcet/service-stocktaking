package com.forcet.service.rest.service.prime.bio.animal;

import com.forcet.service.rest.model.prime.bio.animal.request.SpecieAnimalRequest;
import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SpecieAnimalService {

    ResponseEntity<Summary<SpecieAnimalResponse>> saveSpecieAnimal(SpecieAnimalRequest request);

    ResponseEntity<Summary<SpecieAnimalResponse>> updateSpecieAnimal(SpecieAnimalRequest request);

    ResponseEntity<Summary<SpecieAnimalResponse>> deleteSpecieAnimal(String animalId);

    ResponseEntity<Summary<SpecieAnimalResponse>> deleteAllSpecieAnimal(String code);

    ResponseEntity<Summary<SpecieAnimalResponse>> findSpecieAnimalById(String animalId);

    ResponseEntity<Summary<SpecieAnimalResponse>> findAllSpecieAnimalByUser(String userId);

    ResponseEntity<Summary<List<String>>> saveSpecieAnimalFile(MultipartFile[] files, String userId);

}
