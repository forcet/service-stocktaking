package com.forcet.service.rest.model.prime.bio.vegetable.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SpecieVegetableRequest {


    @ApiModelProperty(position = 1, notes = "${vegetable.description.code}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String vegetableId;

    @ApiModelProperty(position = 2, notes = "${vegetable.description.userCode}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String userCode;

    @ApiModelProperty(position = 3, notes = "${vegetable.description.name}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 4, notes = "${vegetable.description.buyDate}", required = true)
    @NotNull(message = "{default.message.notnull}")
    String buyDate;

    @ApiModelProperty(position = 5, notes = "${vegetable.description.codeMaintenance}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String codeMaintenance;

    @ApiModelProperty(position = 6, notes = "${vegetable.description.cost}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal cost;

    @ApiModelProperty(position = 7, notes = "${vegetable.description.total}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal total;

    @ApiModelProperty(position = 8, notes = "${vegetable.description.diaryConsume}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal diaryConsume;

    @ApiModelProperty(position = 9, notes = "${vegetable.description.loseSpecie}")
    BigDecimal loseSpecie;

    @ApiModelProperty(position = 10, notes = "${vegetable.description.messageLose}")
    List<String> messageLose;

    @ApiModelProperty(position = 11, notes = "${vegetable.description.notifyUser}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String notifyUser;


}
