package com.forcet.service.rest.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Summary<T> {

    @ApiModelProperty(position = 1, notes = "${Summary.time}")
    @JsonProperty("timestamp")
    private LocalDateTime time;

    @ApiModelProperty(position = 2, notes = "${Summary.status}")
    private int status;

    @ApiModelProperty(position = 3, notes = "${Summary.error}")
    private String error;

    @ApiModelProperty(position = 4, notes = "${Summary.message}")
    private String message;

    @ApiModelProperty(position = 5, notes = "${Summary.errorDescriptionList}")
    private List<String> errorDescriptionList;

    private T body;

    private List<T> listBody;

    public Summary() {
    }

    private Summary(int status, String error, String message, List<String> errorDescriptionList, T body) {
        this.time = LocalDateTime.now();
        this.status = status;
        this.error = error;
        this.message = message;
        this.errorDescriptionList = errorDescriptionList;
        this.body = body;
    }

    private Summary(int status, String error, String message, List<String> errorDescriptionList, List<T> listBody) {
        this.time = LocalDateTime.now();
        this.status = status;
        this.error = error;
        this.message = message;
        this.errorDescriptionList = errorDescriptionList;
        this.listBody = listBody;
    }

    public Summary<T> createErrorResponse(List<String> errorDescriptionList, String error, int status) {
        return new Summary<>(status, error, "ERROR", errorDescriptionList, (T) null);
    }

    public Summary<T> createSuccessResponse(T body) {
        return new Summary<>(200, null, "OK", null, body);
    }
    public Summary<T> createSuccessResponse() {
        return new Summary<>(200, null, "OK", null, (T) null);
    }

    public Summary<T> createSuccessResponse(List<T> listBody) {
        return new Summary<>(200, null, "OK", null, listBody);
    }

    public LocalDateTime getTime() {
        return time;
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getErrorDescriptionList() {
        return errorDescriptionList;
    }

    public T getBody() { return body; }

    public List<T> getListBody() { return listBody; }

    public void addErrorDescription(String errorDescription) {
        if (errorDescriptionList == null) {
            errorDescriptionList = new ArrayList<>();
        }
        errorDescriptionList.add(errorDescription);
    }

}
