package com.forcet.service.rest.service.fixed;

import com.forcet.service.rest.model.fixed.response.FixedResponse;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.fixed.FixedRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.service.template.files.BuilderFilesData;
import com.forcet.service.rest.util.ServiceUtil;

import java.util.List;

public class FixedFile extends BuilderFilesData {

    ServiceUtil serviceUtil = new ServiceUtil();

    FixedRepository fixedRepository;


    public FixedFile(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, FixedRepository fixedRepository) {
        super.fileRepository = fileRepository;
        super.messageService = messageService;
        super.userRepository = userRepository;
        this.fixedRepository = fixedRepository;
    }

    public static FixedFile create(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, FixedRepository fixedRepository) {
        return new FixedFile(fileRepository, messageService, userRepository, fixedRepository);
    }



    @Override
    protected void validateProduct(String[] data, String code, List<String> errors) {
        String fixedId = data[0];
        FixedResponse response = fixedRepository.findFixedByUser(code, fixedId).orElse(null);
        if (response == null) {
            fixedRepository.save(FixedResponse
                    .builder()
                    .fixedId(fixedId)
                    .userCode(code)
                    .name(data[1])
                    .quantities(serviceUtil.getBigDecimal(data[2]))
                    .status(data[3])
                    .cost(serviceUtil.getBigDecimal(data[4]))
                    .build());
        } else {
            errors.add(String.format("Elemento ya registrado previamente %s", data[0]));
        }
    }

    @Override
    protected String getTransaction() {
        return "Herramientas";
    }

    @Override
    protected int getSizeFile() {
        return 5;
    }
}
