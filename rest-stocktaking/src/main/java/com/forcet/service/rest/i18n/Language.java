package com.forcet.service.rest.i18n;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Language extends AcceptHeaderLocaleResolver {

    private static final String ACCEPT_LANGUAGE = "Accept-Language";

    List<Locale> locales = Arrays.asList(new Locale("en"),
            new Locale("es"),
            new Locale("es", "EC"));


    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        if (StringUtils.isBlank(request.getHeader(ACCEPT_LANGUAGE))) {
            return Locale.getDefault();
        }
        List<Locale.LanguageRange> list = Locale.LanguageRange.parse(request.getHeader(ACCEPT_LANGUAGE));
        return Locale.lookup(list, locales);
    }

}
