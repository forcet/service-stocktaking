package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.prime.bio.animal.request.SpecieAnimalRequest;
import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.prime.bio.animal.SpecieAnimalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("specieAnimal")
@Api(value = "Creaci\u00F3n de especies animales", description = "API que permite el registro de especies animales", tags = {"Especies Animales/Vegetales"})
public class SpecieAnimalController {

    @Autowired
    SpecieAnimalService specieAnimalService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveSpecieAnimal", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${animal.save.description}")
    public ResponseEntity<Summary<SpecieAnimalResponse>> saveSpecieAnimal(@RequestBody @Valid SpecieAnimalRequest request) {
        return specieAnimalService.saveSpecieAnimal(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllSpecieAnimalByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${animal.findAll.description}")
    public ResponseEntity<Summary<SpecieAnimalResponse>> findAllSpecieAnimalByUser(@RequestParam("userCode") String userCode) {
        return specieAnimalService.findAllSpecieAnimalByUser(userCode);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteSpecieAnimal")
    @ApiOperation(value = "${animal.delelte.description}")
    public ResponseEntity<Summary<SpecieAnimalResponse>> deleteSpecieAnimal(@RequestParam String specieAnimalId) {
        return specieAnimalService.deleteSpecieAnimal(specieAnimalId);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteAllSpecieAnimal")
    @ApiOperation(value = "${animal.delelte.description}")
    public ResponseEntity<Summary<SpecieAnimalResponse>> deleteAllSpecieAnimal(@RequestParam String code) {
        return specieAnimalService.deleteAllSpecieAnimal(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateSpecieAnimal")
    @ApiOperation(value = "${animal.update.description}")
    public ResponseEntity<Summary<SpecieAnimalResponse>> updateSpecieAnimal(@RequestBody @Valid SpecieAnimalRequest request) {
        return  specieAnimalService.updateSpecieAnimal(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/uploadSpecieAnimal",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE })
    @ApiOperation(value = "${file.save.animal.description}")
    public ResponseEntity<Summary<List<String>>> uploadSpecieAnimal(@RequestParam @Valid MultipartFile[] files, @RequestParam String code) {
        return specieAnimalService.saveSpecieAnimalFile(files, code);
    }


}
