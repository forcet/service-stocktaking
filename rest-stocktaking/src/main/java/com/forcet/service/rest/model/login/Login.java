package com.forcet.service.rest.model.login;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Login {

    @ApiModelProperty(position = 1, notes = "${login.description.user}", required = true)
    private String username;
    @ApiModelProperty(position = 2, notes = "${login.description.password}", required = true)
    private String password;
    @ApiModelProperty(hidden = true)
    @Setter private String rol;

}
