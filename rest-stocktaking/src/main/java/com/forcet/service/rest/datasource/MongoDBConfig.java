package com.forcet.service.rest.datasource;


import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import java.util.Collections;



@Configuration
public class MongoDBConfig extends AbstractMongoClientConfiguration {

    @Value("${mongo.db.host}")
    private String host;

    @Value("${mongo.db.port}")
    private int port;

    @Value("${mongo.db.name}")
    private String dbName;

    @Value("${mongo.db.user}")
    private String user;

    @Value("${mongo.db.password}")
    private String password;


   @Override
    public MongoClient mongoClient() {
        MongoCredential credential =  MongoCredential.createCredential(user, "admin", password.toCharArray());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .credential(credential)
                .writeConcern(WriteConcern.ACKNOWLEDGED)
                .readPreference(ReadPreference.primaryPreferred())
                .applyToClusterSettings(settings ->
                        settings.hosts(Collections.singletonList(new ServerAddress(host, port))))
                .build();
        return MongoClients.create(mongoClientSettings);
    }


    @Override
    protected String getDatabaseName() {
        return dbName;
    }
}
