package com.forcet.service.rest.service.prime.bio.animal;

import com.forcet.service.rest.model.prime.bio.animal.request.SpecieAnimalRequest;
import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.prime.bio.animal.SpecieAnimalRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.service.template.files.BuilderFilesData;
import com.forcet.service.rest.util.ServiceUtil;

import java.util.List;

public class AnimalFile extends BuilderFilesData {

    ServiceUtil serviceUtil = new ServiceUtil();

    SpecieAnimalRepository specieAnimalRepository;

    MaintenanceRepository maintenanceRepository;

    public AnimalFile(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, SpecieAnimalRepository specieAnimalRepository, MaintenanceRepository maintenanceRepository) {
        super.fileRepository = fileRepository;
        super.messageService = messageService;
        super.userRepository = userRepository;
        this.specieAnimalRepository = specieAnimalRepository;
        this.maintenanceRepository = maintenanceRepository;
    }

    public static AnimalFile create(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, SpecieAnimalRepository specieAnimalRepository, MaintenanceRepository maintenanceRepository) {
        return new AnimalFile(fileRepository, messageService, userRepository, specieAnimalRepository, maintenanceRepository);
    }


    @Override
    protected void validateProduct(String[] data, String code, List<String> errors) {
        String animalId = data[0];
        SpecieAnimalResponse specieAnimalResponse = specieAnimalRepository.findSpecieAnimalByUser(code, animalId).orElse(null);
        if (specieAnimalResponse == null) {
            SpecieAnimalRequest request = SpecieAnimalRequest.builder()
                    .animalId(animalId)
                    .userCode(code)
                    .name(data[1])
                    .birthDate(data[2])
                    .codeMaintenance(data[3])
                    .cost(serviceUtil.getBigDecimal(data[4]))
                    .total(serviceUtil.getBigDecimal(data[5]))
                    .diaryConsume(serviceUtil.getBigDecimal(data[6]))
                    .notifyUser(data[7])
                    .build();
            SpecieAnimalServiceImpl service = new SpecieAnimalServiceImpl();
            SpecieAnimalResponse response = service.getNewSpecieAnimalResponse(request);
            response.setCodeMaintenanceDescription(getMaintenanceDescription(response.getUserCode(), response.getCodeMaintenance()));
            specieAnimalRepository.save(response);
        } else {
            errors.add(String.format("Elemento ya registrado previamente %s", data[0]));
        }
    }

    private String getMaintenanceDescription(String code, String codeMaintenance) {
        MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(code, codeMaintenance).orElse(null);
        if (maintenanceResponse != null) {
            return maintenanceResponse.getName();
        }

        return null;
    }

    @Override
    protected String getTransaction() {
        return "Especie Animal";
    }

    @Override
    protected int getSizeFile() {
        return 8;
    }
}
