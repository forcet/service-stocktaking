package com.forcet.service.rest.service.message;

import com.forcet.service.rest.model.notification.phone.Phone;

public abstract class TemplateMessage {

    protected Phone phone;

    protected abstract void sendMessage();


    String getFormatNumber() {
        String phoneNumber = phone.getPhoneNumber().substring(1);
        return  String.format("+%s%s",phone.getCodeCountry(), phoneNumber);
    }

}
