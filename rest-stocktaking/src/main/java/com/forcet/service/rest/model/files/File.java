package com.forcet.service.rest.model.files;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
public class File {

    String name;
    @Setter String url;

}
