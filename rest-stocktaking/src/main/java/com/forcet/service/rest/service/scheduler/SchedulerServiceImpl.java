package com.forcet.service.rest.service.scheduler;


import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.scheduler.request.SchedulerRequest;
import com.forcet.service.rest.model.scheduler.response.SchedulerResponse;
import com.forcet.service.rest.model.product.response.ProductResponse;
import com.forcet.service.rest.repository.scheduler.SchedulerRepository;
import com.forcet.service.rest.repository.product.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@Transactional
public class SchedulerServiceImpl implements SchedulerService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    @Autowired
    SchedulerRepository schedulerRepository;

    @Autowired
    ProductRepository productRepository;

    @Override
    public ResponseEntity<Summary<SchedulerResponse>> saveScheduling(SchedulerRequest request) {
        Summary<SchedulerResponse> summary = new Summary<>();
        ResponseEntity<Summary<SchedulerResponse>> responseEntity;
        try {
            SchedulerResponse schedulerResponse = schedulerRepository.findSchedulerByUser(request.getUserCode(), request.getProductId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("Calendarizacion existente"),
                    "Calendarizacion ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (schedulerResponse == null) {
                SchedulerResponse response = schedulerRepository.save(getSchedulerResponse(request, null));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SchedulerResponse>> updateScheduling(SchedulerRequest request) {
        Summary<SchedulerResponse> summary = new Summary<>();
        ResponseEntity<Summary<SchedulerResponse>> responseEntity;
        try {
            SchedulerResponse schedulerResponse = schedulerRepository.findSchedulerByUser(request.getUserCode(), request.getProductId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("Calendarizacion existente"),
                    "Calendarizacion ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (schedulerResponse != null) {
                SchedulerResponse response = schedulerRepository.save(getSchedulerResponse(request, schedulerResponse.getId()));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SchedulerResponse>> deleteScheduling(String code) {
        Summary<SchedulerResponse> summary = new Summary<>();
        ResponseEntity<Summary<SchedulerResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el producto"),
                    "No se encontro la tarea buscada", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            SchedulerResponse schedulerResponse = schedulerRepository.findById(code).orElse(null);
            if (schedulerResponse != null) {
                schedulerRepository.delete(schedulerResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SchedulerResponse>> findByUserAllScheduling(String code) {
        Summary<SchedulerResponse> summary = new Summary<>();
        ResponseEntity<Summary<SchedulerResponse>> responseEntity;
        try {
            List<SchedulerResponse> schedulerResponses = schedulerRepository.findAllSchedulerByUser(code);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(schedulerResponses), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }


    private SchedulerResponse getSchedulerResponse(SchedulerRequest request, String id) {
        return SchedulerResponse.builder()
                .id(id)
                .productId(request.getProductId())
                .userCode(request.getUserCode())
                .nameScheduler(request.getNameScheduler())
                .personalizeMessage(request.getPersonalizeMessage())
                .productName(productDescription(request))
                .startDate(request.getStartDate())
                .endDate(request.getEndDate())
                .notNotifyMail(request.getNotNotifyMail())
                .notNotifyPhone(request.getNotNotifyPhone())
                .build();
    }

    private String productDescription(SchedulerRequest request) {
        ProductResponse product = productRepository.findProductByUser(request.getUserCode(), request.getProductId()).orElse(null);
        if (product != null) {
            return product.getName();
        }
        return "Tarea Personalizada";
    }

}
