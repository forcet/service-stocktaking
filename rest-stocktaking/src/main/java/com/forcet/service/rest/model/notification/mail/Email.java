package com.forcet.service.rest.model.notification.mail;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Email {

    @ApiModelProperty(position = 1, notes = "${email.description.sender}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String sender;
    @ApiModelProperty(position = 2, notes = "${email.description.affair}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String affair;
    @ApiModelProperty(position = 3, notes = "${email.description.body}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String body;

}
