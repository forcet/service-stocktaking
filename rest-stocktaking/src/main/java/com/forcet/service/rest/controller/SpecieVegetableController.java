package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.prime.bio.vegetable.request.SpecieVegetableRequest;
import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.prime.bio.vegetable.SpecieVegetableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("specieVegetable")
@Api(value = "Creaci\u00F3n de especies vegetales", description = "API que permite el registro de especies vegetales", tags = {"Especies Animales/Vegetales"})
public class SpecieVegetableController {

    @Autowired
    SpecieVegetableService specieVegetableService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveSpecieVegetable", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${vegetable.save.description}")
    public ResponseEntity<Summary<SpecieVegetableResponse>> saveSpecieVegetable(@RequestBody @Valid SpecieVegetableRequest request) {
        return specieVegetableService.saveSpecieVegetable(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllSpecieVegetableByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${vegetable.findAll.description}")
    public ResponseEntity<Summary<SpecieVegetableResponse>> findAllSpecieVegetableByUser(@RequestParam("userCode") String userCode) {
        return specieVegetableService.findAllSpecieVegetableByUser(userCode);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteSpecieVegetable")
    @ApiOperation(value = "${vegetable.delelte.description}")
    public ResponseEntity<Summary<SpecieVegetableResponse>> deleteSpecieVegetable(@RequestParam String specieVegetableId) {
        return specieVegetableService.deleteSpecieVegetable(specieVegetableId);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteAllSpecieVegetable")
    @ApiOperation(value = "${vegetable.delelte.description}")
    public ResponseEntity<Summary<SpecieVegetableResponse>> deleteAllSpecieVegetable(@RequestParam String code) {
        return specieVegetableService.deleteAllSpecieVegetable(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateSpecieVegetable")
    @ApiOperation(value = "${vegetable.update.description}")
    public ResponseEntity<Summary<SpecieVegetableResponse>> updateSpecieVegetable(@RequestBody @Valid SpecieVegetableRequest request) {
        return  specieVegetableService.updateSpecieVegetable(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/uploadSpecieVegetable",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE })
    @ApiOperation(value = "${file.save.vegetable.description}")
    public ResponseEntity<Summary<List<String>>> uploadSpecieVegetable(@RequestParam @Valid MultipartFile[] files, @RequestParam String code) {
        return specieVegetableService.saveSpecieVegetableFile(files, code);
    }

}
