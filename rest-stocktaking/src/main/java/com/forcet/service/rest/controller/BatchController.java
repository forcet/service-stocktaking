package com.forcet.service.rest.controller;

import com.forcet.service.rest.model.batch.request.BatchRequest;
import com.forcet.service.rest.model.batch.response.BatchResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.batch.BatchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("batch")
@Api(value = "Creaci\u00F3n de Lotes", description = "API que permite el registro lotes para los productos", tags = {"Lotes"})
public class BatchController {

    @Autowired
    BatchService batchService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveBatch", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${category.save.description}")
    public ResponseEntity<Summary<BatchResponse>> saveBatch(@RequestBody @Valid BatchRequest batchRequest) {
        return batchService.saveBatch(batchRequest);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllBatchByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${category.findAll.description}")
    public ResponseEntity<Summary<BatchResponse>> findAllBatchByUser(@RequestParam("userCode") String userCode) {
        return batchService.findAllBatchByUser(userCode);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteBatch")
    @ApiOperation(value = "${category.delelte.description}")
    public ResponseEntity<Summary<BatchResponse>> deleteBatch(@RequestParam String code) {
        return batchService.deleteBatch(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteAllBatch")
    @ApiOperation(value = "${category.delelte.description}")
    public ResponseEntity<Summary<BatchResponse>> deleteAllBatch(@RequestParam String code) {
        return batchService.deleteAllBatch(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateBatch")
    @ApiOperation(value = "${category.update.description}")
    public ResponseEntity<Summary<BatchResponse>> updateBatch(@RequestBody @Valid BatchRequest batchRequest) {
        return  batchService.updateBatch(batchRequest);
    }

}
