package com.forcet.service.rest.service.prime.bio.vegetable;

import com.forcet.service.rest.model.prime.bio.vegetable.request.SpecieVegetableRequest;
import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SpecieVegetableService {

    ResponseEntity<Summary<SpecieVegetableResponse>> saveSpecieVegetable(SpecieVegetableRequest request);

    ResponseEntity<Summary<SpecieVegetableResponse>> updateSpecieVegetable(SpecieVegetableRequest request);

    ResponseEntity<Summary<SpecieVegetableResponse>> deleteSpecieVegetable(String vegetableId);

    ResponseEntity<Summary<SpecieVegetableResponse>> deleteAllSpecieVegetable(String code);

    ResponseEntity<Summary<SpecieVegetableResponse>> findSpecieVegetableById(String vegetableId);

    ResponseEntity<Summary<SpecieVegetableResponse>> findAllSpecieVegetableByUser(String userId);

    ResponseEntity<Summary<List<String>>> saveSpecieVegetableFile(MultipartFile[] files, String userId);

}
