package com.forcet.service.rest.repository.fixed;

import com.forcet.service.rest.model.fixed.response.FixedResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface FixedRepository extends MongoRepository<FixedResponse, String> {

    @Query("{'userCode' : ?0 }")
    List<FixedResponse> findAllFixedByUser(String userCode);

    @Query("{'userCode' : ?0,  'fixedId' : ?1}")
    Optional<FixedResponse> findFixedByUser(String userCode, String fixedId);

}
