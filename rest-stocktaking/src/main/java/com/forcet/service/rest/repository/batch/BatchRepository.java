package com.forcet.service.rest.repository.batch;

import com.forcet.service.rest.model.batch.response.BatchResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BatchRepository extends MongoRepository<BatchResponse, String> {

    @Query("{'userCode' : ?0 }")
    List<BatchResponse> findAllCategoryByUser(String userCode);

    @Query("{'code' : ?0,  'userCode' : ?1}")
    Optional<BatchResponse> findCategoriesByUser(String code, String userCode);

}
