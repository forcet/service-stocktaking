package com.forcet.service.rest.model.recap;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class Recap {

    @ApiModelProperty(position = 1, notes = "${recap.description.fod}")
    String batchId;

    @ApiModelProperty(position = 1, notes = "${recap.description.fod}")
    String name;

    @ApiModelProperty(position = 2, notes = "${recap.description.quality}")
    BigDecimal quality;

    @ApiModelProperty(position = 4, notes = "${recap.description.qualityActual}")
    BigDecimal qualityActual;

    @ApiModelProperty(position = 5, notes = "${recap.description.cost}")
    BigDecimal cost;



}
