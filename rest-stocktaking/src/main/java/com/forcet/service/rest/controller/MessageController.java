package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.notification.mail.Email;
import com.forcet.service.rest.model.notification.phone.Phone;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.message.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("messages")
@Api(value = "Envio de notificaciones", description = "API que permite el envio de notificaciones", tags = {"Notificaciones"})
public class MessageController {

    @Autowired
    MessageService messageService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/sendMessagePhone", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${message.sendMessagePhone.description}")
    public ResponseEntity<?> sendMessagePhone(@RequestBody @Valid Phone phone) {
        Summary<?> summary = messageService.sendMessagePhone(phone);
        if ("ERROR".equals(summary.getMessage())) {
            return new ResponseEntity<>(summary, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(summary);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/sendEmail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${message.sendEmail.description}")
    public ResponseEntity<?> sendEmail(@RequestBody @Valid Email email) {
        Summary<?> summary = messageService.sendMessageEmail(email);
        if ("ERROR".equals(summary.getMessage())) {
            return new ResponseEntity<>(summary, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(summary);
    }

}
