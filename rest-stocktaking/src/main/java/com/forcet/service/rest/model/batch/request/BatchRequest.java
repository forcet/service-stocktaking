package com.forcet.service.rest.model.batch.request;


import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BatchRequest {

    @ApiModelProperty(position = 1, notes = "${batch.description.code}", required = true)
    String code;
    @ApiModelProperty(position = 2, notes = "${batch.description.userCode}", required = true)
    String userCode;
    @ApiModelProperty(position = 3, notes = "${batch.description.name}", required = true)
    String name;
    @ApiModelProperty(position = 4, notes = "${batch.description.description}", required = true)
    String description;
    @ApiModelProperty(position = 5, notes = "${batch.description.type}", required = true)
    String type;


}
