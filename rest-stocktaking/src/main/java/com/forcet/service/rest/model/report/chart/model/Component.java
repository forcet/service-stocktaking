package com.forcet.service.rest.model.report.chart.model;

import java.util.HashMap;
import java.util.Map;

public enum Component {


    PRODUCT("product"),
    MAINTENANCE("maintenance"),
    SPECIE_VEGETABLE("specieVegetable"),
    SPECIE_ANIMAL("specieAnimal"),
    FIXED("fixed"),
    RESULT("result"),
    DEFAULT("default");

    private static final Map<String, Component> codeMap = new HashMap<>();

    static {
        for (Component tables : values()) {
            codeMap.put(tables.getComponent(), tables);
        }
    }

    private final String code;

    Component(String code) {
        this.code = code;
    }

    public static Component fromCode(String code) {
        if (codeMap.get(code) == null) {
            return codeMap.get("default");
        }
        return codeMap.get(code);
    }

    public String getComponent() {
        return code;
    }

}
