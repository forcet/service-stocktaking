package com.forcet.service.rest.service.batch;

import com.forcet.service.rest.model.batch.request.BatchRequest;
import com.forcet.service.rest.model.batch.response.BatchResponse;
import com.forcet.service.rest.model.response.Summary;

import com.forcet.service.rest.repository.batch.BatchRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@Transactional
public class BatchServiceImpl implements BatchService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";
    private static final String ERROR_NO_EXIST = "No se encontro la categoria buscada";

    @Autowired
    BatchRepository repository;

    @Override
    public ResponseEntity<Summary<BatchResponse>> saveBatch(BatchRequest batchRequest) {
        Summary<BatchResponse> summary = new Summary<>();
        ResponseEntity<Summary<BatchResponse>> responseEntity;
        try {
            BatchResponse previewBatchResponse = repository.findCategoriesByUser(batchRequest.getCode(), batchRequest.getUserCode()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("La categoria ya existe"),
                    "La categoria ingresada ya se encuentra registrada", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (previewBatchResponse == null ) {
                BatchResponse response = getNewBatchResponse(batchRequest);
                repository.save(response);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<BatchResponse>> updateBatch(BatchRequest batchRequest) {
        Summary<BatchResponse> summary = new Summary<>();
        ResponseEntity<Summary<BatchResponse>> responseEntity;
        try {
            BatchResponse previewBatchResponse = repository.findCategoriesByUser(batchRequest.getCode(), batchRequest.getUserCode()).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ninguna categoria"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (previewBatchResponse != null) {
                BatchResponse updateBatch = repository.save(getUpdateBatchResponse(batchRequest, previewBatchResponse));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(updateBatch), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<BatchResponse>> deleteBatch(String code) {
        Summary<BatchResponse> summary = new Summary<>();
        ResponseEntity<Summary<BatchResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe la categoria"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            BatchResponse batchResponse = repository.findById(code).orElse(null);
            if (batchResponse != null) {
                repository.delete(batchResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<BatchResponse>> deleteAllBatch(String userCode) {
        Summary<BatchResponse> summary = new Summary<>();
        ResponseEntity<Summary<BatchResponse>> responseEntity;
        try {
            List<BatchResponse> responses = repository.findAllCategoryByUser(userCode);
            repository.deleteAll(responses);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<BatchResponse>> findBatchById(String code) {
        Summary<BatchResponse> summary = new Summary<>();
        ResponseEntity<Summary<BatchResponse>> responseEntity;
        try {

            BatchResponse batchResponse = repository.findById(code).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe la categoria"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (batchResponse != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(batchResponse), HttpStatus.OK);
            }

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<BatchResponse>> findAllBatchByUser(String userCode) {
        Summary<BatchResponse> summary = new Summary<>();
        ResponseEntity<Summary<BatchResponse>> responseEntity;
        try {
            List<BatchResponse> batchResponses = repository.findAllCategoryByUser(userCode);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(batchResponses), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    private BatchResponse getNewBatchResponse(BatchRequest request) {
        return BatchResponse.builder()
                .code(request.getCode())
                .userCode(request.getUserCode())
                .name(request.getName())
                .description(request.getDescription())
                .registerDate(LocalDate.now())
                .build();

    }

    private BatchResponse getUpdateBatchResponse(BatchRequest request, BatchResponse response) {
        return BatchResponse.builder()
                .id(response.getId())
                .code(response.getCode())
                .userCode(response.getUserCode())
                .name(request.getName())
                .description(request.getDescription())
                .registerDate(response.getRegisterDate())
                .build();

    }

}
