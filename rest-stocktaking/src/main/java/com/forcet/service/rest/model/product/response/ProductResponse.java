package com.forcet.service.rest.model.product.response;


import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
@Document(collection = "product")
public class ProductResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${product.description.id}")
    private String id;

    @ApiModelProperty(position = 2, notes = "${product.description.productId}")
    String productId; //Codigo producto

    @ApiModelProperty(position = 3, notes = "${product.description.username}")
    String username; //Codigo Usuario

    @ApiModelProperty(position = 4, notes = "${product.description.name}")
    String name; //Nombre producto

    @ApiModelProperty(position = 5, notes = "${product.description.realPrice}")
    BigDecimal realPrice; //Precio real

    @ApiModelProperty(position = 6, notes = "${product.description.marketPrice}")
    BigDecimal marketPrice; //Precio de venta en el mercado

    @ApiModelProperty(position = 7, notes = "${product.description.quantity}")
    BigDecimal quantity; // Cantidad

    @ApiModelProperty(position = 8, notes = "${product.description.sellQuantity}")
    BigDecimal sellQuantity; // Cantidad vendida

    @ApiModelProperty(position = 9, notes = "${product.description.releaseDate}")
    String releaseDate; // fecha de liberacion

    @ApiModelProperty(position = 10, notes = "${product.description.loseProduct}")
    BigDecimal loseProduct; //Perdida de producto

    @ApiModelProperty(position = 11, notes = "${product.description.messageLose}")
    List<String> messageLose; // Nota de perdida

    @ApiModelProperty(position = 12, notes = "${product.description.rejection}")
    BigDecimal rejection; //Cantidad menos la cantidad vendia

    @ApiModelProperty(position = 13, notes = "${product.description.realGreed}")
    BigDecimal realGreed; // Precio real y cantidad vendida

    @ApiModelProperty(position = 14, notes = "${product.description.estimateGreed}")
    BigDecimal estimateGreed; //Precio de mercado y cantidad

    @ApiModelProperty(position = 15, notes = "${product.description.antiquity}")
    BigDecimal antiquity; //Dias

}
