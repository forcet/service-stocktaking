package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.files.File;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.files.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("files")
@Api(value = "Manejo de archivos", description = "API que permite manejar archivos", tags = {"Archivos"})
public class FileController {

    private static final String ERROR = "ERROR";

    @Autowired
    FileService fileService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/upload",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE })
    @ApiOperation(value = "${file.save.description}")
    public ResponseEntity<?> saveFile(@RequestParam @Valid MultipartFile[] files, @RequestParam String code) {
        Summary<?> summary = fileService.save(files, code);
        if (ERROR.equals(summary.getMessage())) {
            return new ResponseEntity<>(summary, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(summary);
    }


    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/file", produces = "text/plain")
    @ApiOperation(value = "${files.description.file}")
    public ResponseEntity<?> getFile(@RequestParam String file, @RequestParam String code) {
        Summary<Resource> summary = fileService.load(file, code);
        if (ERROR.equals(summary.getMessage())) {
             return new ResponseEntity<>(summary, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok()
                .header(
                HttpHeaders.CONTENT_DISPOSITION, "attachment; file=\""+summary.getBody().getFilename()+ "\"")
                .contentType(MediaType.parseMediaType("text/plain"))
                .cacheControl(CacheControl.noCache())
                .body(summary.getBody());
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/files", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${files.description.files}")
    public ResponseEntity<Summary<List<File>>> getFiles(@RequestParam String code) {
        Summary<List<File>> summary = fileService.loadAll(code);
        if (ERROR.equals(summary.getMessage())) {
            return new ResponseEntity<>(summary, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        List<File> fileList = summary.getBody();
        fileList.forEach(file -> {
            String url = MvcUriComponentsBuilder.fromMethodName(FileController.class, "getFile",
                    file.getName(), code).toUriString();
            file.setUrl(url);
        });
        return ResponseEntity.ok(summary);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteFile")
    @ApiOperation(value = "${user.delelte.description}")
    public ResponseEntity<?> deleteUser(@RequestParam String file, @RequestParam String code) {
        Summary<?> summary = fileService.deleteFile(file, code);
        if (ERROR.equals(summary.getMessage())) {
            return new ResponseEntity<>(summary, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(summary);
    }


}
