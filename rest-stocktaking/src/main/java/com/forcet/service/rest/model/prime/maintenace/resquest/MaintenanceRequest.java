package com.forcet.service.rest.model.prime.maintenace.resquest;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaintenanceRequest {

    @ApiModelProperty(position = 1, notes = "${maintenance.description.id}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String maintenanceId; // Codigo Maintenimance

    @ApiModelProperty(position = 2, notes = "${maintenance.description.userCode}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String userCode; //Codigo de Usuario

    @ApiModelProperty(position = 3, notes = "${maintenance.description.name}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String name; //Nombre

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 4, notes = "${maintenance.description.elaborateDate}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String elaborateDate; // Fecha de elaboracion

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 5, notes = "${maintenance.description.expirationDate}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String expirationDate; //Fecha de expiracion

    @ApiModelProperty(position = 6, notes = "${maintenance.description.cost}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal cost; // Costo

    @ApiModelProperty(position = 7, notes = "${maintenance.description.measurement}")
    String measurement; // Unidad de medida

    @ApiModelProperty(position = 8, notes = "${maintenance.description.quantity}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal quantity; // Cantidad

    @ApiModelProperty(position = 9, notes = "${maintenance.description.lostQuantity}")
    BigDecimal lostQuantity; // Cantidad perdida

    @ApiModelProperty(position = 10, notes = "${maintenance.description.batchId}", required = true)
    @NotNull(message = "{default.message.notnull}")
    String batchId;

    @ApiModelProperty(position = 11, notes = "${maintenance.description.notifyUser}")
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String notifyUser ; // Desea Notificaciones

    @ApiModelProperty(position = 12, notes = "${maintenance.description.lostQuantity}")
    List<String> messageLose;




}
