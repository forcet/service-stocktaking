package com.forcet.service.rest.service.prime.maintenance;

import com.forcet.service.rest.model.batch.response.BatchResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.prime.maintenace.resquest.MaintenanceRequest;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.repository.batch.BatchRepository;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service
@Transactional
public class MaintenanceServiceImpl implements MaintenanceService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    private static final String ERROR_NO_EXIST = "No se encontro el elemento buscado";

    ServiceUtil serviceUtil = new ServiceUtil();

    @Autowired
    MaintenanceRepository maintenanceRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    BatchRepository batchRepository;

    @Autowired
    MessageService messageService;

    @Autowired
    UserRepository userRepository;


    @Override
    public ResponseEntity<Summary<MaintenanceResponse>> saveMaintenance(MaintenanceRequest request) {
        Summary<MaintenanceResponse> summary = new Summary<>();
        ResponseEntity<Summary<MaintenanceResponse>> responseEntity;
        try {
            MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(request.getUserCode(), request.getMaintenanceId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("El elemento no existe"),
                    "El elemento ingresado ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (maintenanceResponse == null ) {
                MaintenanceResponse response = maintenanceRepository.save(getNewMaintenanceResponse(request));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<MaintenanceResponse>> updateMaintenance(MaintenanceRequest request) {
        Summary<MaintenanceResponse> summary = new Summary<>();
        ResponseEntity<Summary<MaintenanceResponse>> responseEntity;
        try {
            MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(request.getUserCode(), request.getMaintenanceId()).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun elemento"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (maintenanceResponse != null) {
                MaintenanceResponse updateMaintenance = maintenanceRepository.save(getUpdateMaintenanceResponse(request, maintenanceResponse));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(updateMaintenance), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<MaintenanceResponse>> deleteMaintenance(String maintenanceId) {
        Summary<MaintenanceResponse> summary = new Summary<>();
        ResponseEntity<Summary<MaintenanceResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el elemento"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            MaintenanceResponse maintenanceResponse = maintenanceRepository.findById(maintenanceId).orElse(null);
            if (maintenanceResponse != null) {
                maintenanceRepository.delete(maintenanceResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<MaintenanceResponse>> deleteAllMaintenance(String code) {
        Summary<MaintenanceResponse> summary = new Summary<>();
        ResponseEntity<Summary<MaintenanceResponse>> responseEntity;
        try {
            List<MaintenanceResponse> responses = maintenanceRepository.findAllMaintenanceByUser(code);
            maintenanceRepository.deleteAll(responses);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<MaintenanceResponse>> findMaintenanceById(String maintenanceId) {
        Summary<MaintenanceResponse> summary = new Summary<>();
        ResponseEntity<Summary<MaintenanceResponse>> responseEntity;
        try {
            MaintenanceResponse maintenanceResponse = maintenanceRepository.findById(maintenanceId).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el elemento"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (maintenanceResponse != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(maintenanceResponse), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<MaintenanceResponse>> findAllMaintenanceByUser(String userId) {
        Summary<MaintenanceResponse> summary = new Summary<>();
        ResponseEntity<Summary<MaintenanceResponse>> responseEntity;
        try {
            List<MaintenanceResponse> product = maintenanceRepository.findAllMaintenanceByUser(userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(product), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<List<String>>> saveMaintenanceFile(MultipartFile[] files, String userId) {
        Summary<List<String>> summary = new Summary<>();
        ResponseEntity<Summary<List<String>>> responseEntity;
        try {
            List<String> errors = MaintenanceFile.create(fileRepository, messageService,
                    userRepository, maintenanceRepository, batchRepository).saveUserFiles(files, userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(errors), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    public MaintenanceResponse getNewMaintenanceResponse(MaintenanceRequest request) {
        LocalDate elaborateDate = serviceUtil.getLocalDate(request.getElaborateDate());
        LocalDate expireDate = serviceUtil.getLocalDate(request.getExpirationDate());
        long age = serviceUtil.getDaysCountBetweenDates(elaborateDate, null);
        long duration = serviceUtil.getDaysCountBetweenDates(elaborateDate, expireDate);
        return MaintenanceResponse.builder()
                .maintenanceId(request.getMaintenanceId())
                .name(request.getName())
                .userCode(request.getUserCode())
                .elaborateDate(dateFormat(request.getElaborateDate()))
                .expirationDate(dateFormat(request.getExpirationDate()))
                .cost(request.getCost())
                .measurement(request.getMeasurement())
                .quantity(request.getQuantity())
                .age(new BigDecimal(age))
                .duration(new BigDecimal(duration))
                .quantityActual(request.getQuantity())
                .notifyUser(request.getNotifyUser())
                .batchId(request.getBatchId())
                .batchDescription(getBatchName(request.getBatchId(), request.getUserCode()))
                .build();

    }

    private MaintenanceResponse getUpdateMaintenanceResponse(MaintenanceRequest request, MaintenanceResponse response) {
        LocalDate elaborateDate = serviceUtil.getLocalDate(request.getElaborateDate());
        LocalDate expireDate = serviceUtil.getLocalDate(request.getExpirationDate());
        long age = serviceUtil.getDaysCountBetweenDates(elaborateDate, null);
        long duration = serviceUtil.getDaysCountBetweenDates(elaborateDate, expireDate);
        return MaintenanceResponse.builder()
                .id(response.getId())
                .maintenanceId(response.getMaintenanceId())
                .userCode(response.getUserCode())
                .name(request.getName())
                .elaborateDate(dateFormat(request.getElaborateDate()))
                .expirationDate(dateFormat(request.getExpirationDate()))
                .cost(request.getCost())
                .measurement(request.getMeasurement())
                .quantity(request.getQuantity())
                .age(new BigDecimal(age))
                .duration(new BigDecimal(duration))
                .quantityActual(request.getQuantity())
                .notifyUser(request.getNotifyUser())
                .batchId(request.getBatchId())
                .batchDescription(getBatchName(request.getBatchId(), request.getUserCode()))
                .build();

    }

    private String dateFormat(String date) {
        LocalDate localDate = serviceUtil.getLocalDate(date);
        return serviceUtil.getStringDate(localDate);
    }

    private String getBatchName(String code, String user) {
        if(Optional.ofNullable(batchRepository).isPresent()) {
            return batchRepository.findCategoriesByUser(code, user).orElse(BatchResponse.builder().build()).getName();
        }
       return null;
    }


}
