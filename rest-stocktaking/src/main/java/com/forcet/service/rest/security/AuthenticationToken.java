package com.forcet.service.rest.security;

import com.forcet.service.rest.model.login.Login;
import com.forcet.service.rest.model.user.response.UserResponse;
import com.forcet.service.rest.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationToken implements AuthenticationManager {

    @Value("${http.auth.token}")
    private String apiRequest;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Login info = (Login) authentication.getCredentials();
        if (!isAuthenticate(info)) {
            throw new BadCredentialsException("The API key was not found or not the expected value.");
        }
        return authentication;
    }

    private boolean isAuthenticate(Login login) {
        UserResponse userResponse = userRepository.findById(login.getUsername()).orElse(null);
        if (userResponse != null) {
            login.setRol(userResponse.getRol());
            return userResponse.checkPassword(login.getPassword());
        }
        return Boolean.FALSE;
    }
}
