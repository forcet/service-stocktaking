package com.forcet.service.rest.exception;


import com.forcet.service.rest.model.response.Summary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.util.Collections;

@ControllerAdvice
public class FIleUploadException {

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<?> maxSizeException() {
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new Summary<>()
                        .createErrorResponse(
                                Collections.singletonList("Tamaño máximo excedido"),
                                "Uno o mas archivos exceden el tamaño máximo",
                                HttpStatus.EXPECTATION_FAILED.value()));
    }
}
