package com.forcet.service.rest.service.prime.maintenance;

import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.prime.maintenace.resquest.MaintenanceRequest;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MaintenanceService {


    ResponseEntity<Summary<MaintenanceResponse>> saveMaintenance(MaintenanceRequest request);

    ResponseEntity<Summary<MaintenanceResponse>> updateMaintenance(MaintenanceRequest request);

    ResponseEntity<Summary<MaintenanceResponse>> deleteMaintenance(String maintenanceId);

    ResponseEntity<Summary<MaintenanceResponse>> deleteAllMaintenance(String code);

    ResponseEntity<Summary<MaintenanceResponse>> findMaintenanceById(String maintenanceId);

    ResponseEntity<Summary<MaintenanceResponse>> findAllMaintenanceByUser(String userId);

    ResponseEntity<Summary<List<String>>> saveMaintenanceFile(MultipartFile[] files, String userId);

}
