package com.forcet.service.rest.model.scheduler.request;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Getter
@Setter
public class SchedulerRequest {

    @ApiModelProperty(position = 1, notes = "${scheduler.description.userCode}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String userCode;

    @ApiModelProperty(position = 2, notes = "${scheduler.description.productId}", required = true)
    @NotNull(message = "{default.message.productId}")
    private String productId;

    @ApiModelProperty(position = 3, notes = "${scheduler.description.productId}", required = true)
    @NotNull(message = "{default.message.productId}")
    private String nameScheduler;

    @ApiModelProperty(position = 4, notes = "${scheduler.description.personalizeMessage}")
    private String personalizeMessage;

    @ApiModelProperty(position = 5, notes = "${scheduler.description.startDate}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private LocalDate startDate;

    @ApiModelProperty(position = 6, notes = "${scheduler.description.endDate}")
    private LocalDate endDate;

    @ApiModelProperty(position = 7, notes = "${scheduler.description.notNotifyMail}")
    private String notNotifyMail;

    @ApiModelProperty(position = 8, notes = "${scheduler.description.notNotifyPhone}")
    private String notNotifyPhone;


}
