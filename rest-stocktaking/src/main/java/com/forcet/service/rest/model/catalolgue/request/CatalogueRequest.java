package com.forcet.service.rest.model.catalolgue.request;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CatalogueRequest {

    @ApiModelProperty(position = 1, notes = "${catalogue.description.code}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String code;
    @ApiModelProperty(position = 2, notes = "${catalogue.description.value}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String value;
}
