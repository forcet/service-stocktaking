package com.forcet.service.rest.service.reports.template;

import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.report.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MaintenanceReport extends FillReports {

    List<MaintenanceResponse> listField;

    public MaintenanceReport(List<MaintenanceResponse> listField) {
        this.listField = listField;
    }

    public static MaintenanceReport create(List<MaintenanceResponse> listField) {
        return new MaintenanceReport(listField);
    }

    @Override
    protected Field changeFieldReport() {
        List<MaintenanceResponse> responseList = this.listField;
        List<List<Object>> rowList = responseList.stream().map(response -> {
            List<Object> row = new ArrayList<>();
            row.add(response.getMaintenanceId());
            row.add(response.getName());
            row.add(response.getElaborateDate());
            row.add(response.getExpirationDate());
            row.add(response.getCost());
            row.add(response.getMeasurement());
            row.add(response.getQuantity());
            row.add(response.getQuantityActual());
            row.add(response.getBatchId());
            return row;
        }).collect(Collectors.toList());
        return Field.builder()
                .columnName(Arrays.asList("Código","Nombre", "Fecha Elaboración", "Fecha Expiración", "Costo", "Unidad de medida", "Cantidad aqquirida", "Cantidad Actual" ))
                .valueRows(rowList)
                .build() ;

    }
}
