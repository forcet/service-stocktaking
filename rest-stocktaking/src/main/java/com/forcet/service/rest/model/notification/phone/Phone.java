package com.forcet.service.rest.model.notification.phone;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Phone {

    @ApiModelProperty(position = 1, notes = "${phone.description.codeCountry}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String codeCountry;
    @ApiModelProperty(position = 2, notes = "${phone.description.phoneNumber}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String phoneNumber;
    @ApiModelProperty(position = 3, notes = "${phone.description.message}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private String message;

}
