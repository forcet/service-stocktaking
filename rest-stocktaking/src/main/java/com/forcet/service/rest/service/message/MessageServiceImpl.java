package com.forcet.service.rest.service.message;

import com.forcet.service.rest.model.notification.mail.Email;
import com.forcet.service.rest.model.notification.phone.Phone;
import com.forcet.service.rest.model.response.Summary;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.Collections;

@Slf4j
@Service
public class MessageServiceImpl implements MessageService {

    @Value("${twilio.account.sid}")
    private String accountSid;
    @Value("${twilio.auth.token}")
    private String authToken;
    @Value("${number.twilio}")
    private String numberTwilio;

    @Value("${spring.mail.username}")
    private String from;

    @Value("${access.notification}")
    private String accessKey;
    @Value("${key.notification}")
    private String secretKey;

    private static final String ERROR_GENERIC = "Ha ocurrido un error ";


    @Autowired
    private JavaMailSender mailSender;

    @Override
    public Summary<?> sendMessageEmail(Email email) {
        Summary<?> summary = new Summary<>();
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setFrom(from, "Inventario");
            mimeMessageHelper.setSubject(email.getAffair());
            mimeMessageHelper.setTo(email.getSender());
            mimeMessageHelper.setText(email.getBody(), true);
            mailSender.send(mimeMessage);
            summary = summary.createSuccessResponse();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;

    }

    @Override
    public Summary<?> sendMessagePhone(Phone phone) {
        Summary<?> summary = new Summary<>();
        try {
            //MessageTwilio.create(phone, accountSid, authToken, numberTwilio).sendMessage();
            MessageAws.create(phone, accessKey, secretKey).sendMessage();
            summary = summary.createSuccessResponse();
        }catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;
    }

}
