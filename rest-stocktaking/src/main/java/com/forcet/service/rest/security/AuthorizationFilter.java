package com.forcet.service.rest.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    public AuthorizationFilter(AuthenticationManager manager) {
        super(manager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(Constants.HEADER_STRING);
        if (StringUtils.isEmpty(header) || !header.startsWith(Constants.TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }
        UsernamePasswordAuthenticationToken passwordAuthenticationToken = getAuthenticationToken(header);
        SecurityContextHolder.getContext().setAuthentication(passwordAuthenticationToken);
        chain.doFilter(request, response);
    }


    private UsernamePasswordAuthenticationToken getAuthenticationToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(Constants.SECRET.getBytes(Charset.defaultCharset()))
                .parseClaimsJws(token.replace(Constants.TOKEN_PREFIX, ""))
                .getBody();
        String user = claims.getSubject();
        return getAuthorities(claims, user);
    }

    private UsernamePasswordAuthenticationToken getAuthorities(Claims claims, String code) {
        if (StringUtils.isNotEmpty(code) && claims.get("authorities") != null) {
            Collection<SimpleGrantedAuthority> authorities = Arrays.stream(claims.get("authorities").toString()
                    .split(","))
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());

            return new UsernamePasswordAuthenticationToken(code, null, authorities);
        }
        return null;
    }
}
