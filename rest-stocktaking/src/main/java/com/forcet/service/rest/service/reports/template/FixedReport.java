package com.forcet.service.rest.service.reports.template;

import com.forcet.service.rest.model.fixed.response.FixedResponse;
import com.forcet.service.rest.model.report.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FixedReport extends FillReports {

    List<FixedResponse> listField;

    public FixedReport(List<FixedResponse> listField) {
        this.listField = listField;
    }

    public static FixedReport create(List<FixedResponse> listField) {
        return new FixedReport(listField);
    }

    @Override
    protected Field changeFieldReport() {
        List<FixedResponse> responseList = this.listField;
        List<List<Object>> rowList = responseList.stream().map(response -> {
            List<Object> row = new ArrayList<>();
            row.add(response.getFixedId());
            row.add(response.getName());
            row.add(response.getQuantities());
            row.add(response.getStatus());
            row.add(response.getCost());
            return row;
        }).collect(Collectors.toList());
        return Field.builder()
                .columnName(Arrays.asList("Código","Nombre", "Cantidad", "Estado", "Costo" ))
                .valueRows(rowList)
                .build() ;
    }
}
