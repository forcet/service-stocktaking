package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.user.request.UserRequest;
import com.forcet.service.rest.model.user.response.UserResponse;
import com.forcet.service.rest.service.user.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("users")
@Api(value = "Creaci\u00F3n de Personas/Usuarios", description = "API que permite el registro de Usuarios", tags = {"Personas/Usuarios"})
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/saveUser", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${user.save.description}")
    public ResponseEntity<Summary<UserResponse>> saveUser(@RequestBody @Valid UserRequest userRequest) {
        return userService.saveUser(userRequest);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${user.findAll.description}")
    public ResponseEntity<Summary<UserResponse>> findAllUser() {
        return userService.findAll();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/deleteUser")
    @ApiOperation(value = "${user.delelte.description}")
    public ResponseEntity<Summary<UserResponse>> deleteUser(@RequestParam String codeUser) {
        return userService.deleteUser(codeUser);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping(value = "/updateUser", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${user.update.description}")
    public ResponseEntity<Summary<UserResponse>> updateUser(@RequestBody @Valid UserRequest userRequest) {
        return userService.updateUser(userRequest);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/findUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${user.update.description}")
    public ResponseEntity<Summary<UserResponse>> findUser(@RequestParam("userCode") String userCode) {
        return userService.findById(userCode);
    }

}
