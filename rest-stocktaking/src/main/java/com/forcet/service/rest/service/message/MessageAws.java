package com.forcet.service.rest.service.message;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.forcet.service.rest.model.notification.phone.Phone;

public class MessageAws extends TemplateMessage {


    private String accessKey;
    private String secretKey;


    public MessageAws(Phone phone, String accessKey, String secretKey) {
        super.phone = phone;
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    public static MessageAws create(Phone phone, String accessKey, String secretKey) {
        return new MessageAws(phone, accessKey, secretKey);
    }


    @Override
    public void sendMessage() {
     BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
     AmazonSNS snsClient = AmazonSNSClient
             .builder()
             .withRegion(Regions.US_EAST_2)
             .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
             .build();
     String number = getFormatNumber();
     PublishResult request = snsClient.publish(new PublishRequest().withMessage(phone.getMessage()).withPhoneNumber(number));
     request.getSequenceNumber();

 }

}
