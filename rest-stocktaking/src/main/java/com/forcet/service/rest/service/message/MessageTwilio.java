package com.forcet.service.rest.service.message;

import com.forcet.service.rest.model.notification.phone.Phone;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class MessageTwilio extends TemplateMessage {

    private String accountSid;

    private String authToken;

    private String numberTwilio;

    public MessageTwilio(Phone phone, String accountSid, String authToken, String numberTwilio) {
        super.phone = phone;
        this.accountSid = accountSid;
        this.authToken = authToken;
        this.numberTwilio = numberTwilio;
    }

    public static MessageTwilio create(Phone phone, String accountSid, String authToken, String numberTwilio) {
        return new MessageTwilio(phone, accountSid, authToken, numberTwilio);
    }


    @Override
    protected void sendMessage() {
        Twilio.init(accountSid, authToken);
        String number = getFormatNumber();
        Message.creator(new PhoneNumber(number),
                new PhoneNumber(numberTwilio),
                phone.getMessage()).create();
    }
}
