package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.scheduler.request.SchedulerRequest;
import com.forcet.service.rest.model.scheduler.response.SchedulerResponse;
import com.forcet.service.rest.service.scheduler.SchedulerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("scheduler")
@Api(value = "Creaci\u00F3n de Parametrizaci\u00F3n Tareas Calendarizadas", description = "API que permite el registro de Tareas", tags = {"Scheduler"})
public class SchedulerController {

    @Autowired
    SchedulerService schedulerService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PostMapping(value = "/saveScheduler", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${scheduler.save.description}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Summary.class)})
    public ResponseEntity<Summary<SchedulerResponse>> saveScheduler(@RequestBody @Valid SchedulerRequest request) {
        return schedulerService.saveScheduling(request);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findAllSchedulerByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${scheduler.findAll.description}")
    public ResponseEntity<Summary<SchedulerResponse>> findAllSchedulerByUser(@RequestParam("code") String code) {
        return schedulerService.findByUserAllScheduling(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @DeleteMapping(value = "/deleteScheduler")
    @ApiOperation(value = "${scheduler.delelte.description}")
    public ResponseEntity<Summary<SchedulerResponse>> deleteScheduler(@RequestParam String code) {
        return schedulerService.deleteScheduling(code);
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @PatchMapping(value = "/updateScheduler")
    @ApiOperation(value = "${scheduler.update.description}")
    public ResponseEntity<Summary<SchedulerResponse>> updateScheduler(@RequestBody @Valid SchedulerRequest request) {
        return schedulerService.updateScheduling(request);
    }

}
