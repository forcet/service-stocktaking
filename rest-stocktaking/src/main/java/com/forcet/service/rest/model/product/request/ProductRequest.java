package com.forcet.service.rest.model.product.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @ApiModelProperty(position = 1, notes = "${product.description.productId}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String productId; //Codigo producto

    @ApiModelProperty(position = 2, notes = "${product.description.username}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String username; //Codigo Usuario

    @ApiModelProperty(position = 3, notes = "${product.description.name}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String name; //Nombre producto

    @ApiModelProperty(position = 4, notes = "${product.description.realPrice}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal realPrice; //Precio real

    @ApiModelProperty(position = 5, notes = "${product.description.marketPrice}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal marketPrice; //Precio de venta en el mercado

    @ApiModelProperty(position = 6, notes = "${product.description.quantity}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal quantity; // Cantidad

    @ApiModelProperty(position = 7, notes = "${product.description.sellQuantity}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal sellQuantity; // Cantidad vendida

    @ApiModelProperty(position = 8, notes = "${product.description.releaseDate}", required = true)
    @NotNull(message = "{default.message.notnull}")
    String releaseDate; // fecha de liberacion

    @ApiModelProperty(position = 9, notes = "${product.description.loseProduct}")
    BigDecimal loseProduct; //Perdida de producto

    @ApiModelProperty(position = 10, notes = "${product.description.messageLose}")
    List<String> messageLose; // Nota de perdida

}
