package com.forcet.service.rest.service.product;


import com.forcet.service.rest.model.product.request.ProductRequest;
import com.forcet.service.rest.model.product.response.ProductResponse;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.product.ProductRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.service.template.files.BuilderFilesData;
import com.forcet.service.rest.util.ServiceUtil;


import java.util.List;

public class ProductFile extends BuilderFilesData {

    ProductRepository productRepository;

    ServiceUtil serviceUtil = new ServiceUtil();


    public ProductFile(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, ProductRepository productRepository) {
        super.fileRepository = fileRepository;
        super.messageService = messageService;
        super.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    public static ProductFile create(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, ProductRepository productRepository) {
        return new ProductFile(fileRepository, messageService, userRepository, productRepository);
    }

    @Override
    protected void validateProduct(String[] data, String code, List<String> errors) {
        String productId = data[0];
        ProductResponse previewProduct = productRepository.findProductByUser(code, productId).orElse(null);
        if (previewProduct == null) {
            ProductRequest request = ProductRequest.builder()
                    .productId(productId)
                    .username(code)
                    .name(data[1])
                    .realPrice(serviceUtil.getBigDecimal(data[2]))
                    .marketPrice(serviceUtil.getBigDecimal(data[3]))
                    .quantity(serviceUtil.getBigDecimal(data[4]))
                    .sellQuantity(serviceUtil.getBigDecimal(data[5]))
                    .releaseDate(data[6])
                    .build();
            ProductServiceImpl service = new ProductServiceImpl();
            ProductResponse response = service.getNewProductResponse(request);
            productRepository.save(response);
        } else {
            errors.add(String.format("Producto ya registrado previamente %s", data[0]));
        }
    }

    @Override
    protected String getTransaction() {
        return "Productos";
    }

    @Override
    protected int getSizeFile() {
        return 7;
    }


}
