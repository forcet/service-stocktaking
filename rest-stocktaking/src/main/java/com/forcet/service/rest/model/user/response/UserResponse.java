package com.forcet.service.rest.model.user.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.forcet.service.rest.model.user.Person;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import java.time.LocalDateTime;


@Getter
@Setter
@Builder
@Document(collection = "user")
public class UserResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${data.user.nickName}")
    private String username;

    @ApiModelProperty(position = 2, notes = "${data.user.password}")
    private String password;

    @ApiModelProperty(position = 3, notes = "${data.user.status}")
    private String status;

    @ApiModelProperty(position = 4, notes = "${data.user.rol}")
    private String rol;

    @ApiModelProperty(position = 5, notes = "${data.user.person}", example = "yyyy/MM/dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private LocalDateTime creationDate;

    @ApiModelProperty(position = 6, notes = "${data.user.person}",example = "yyyy/MM/dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private LocalDateTime updateDate;

    @ApiModelProperty(position = 7, notes = "${data.user.person}")
    private Person person;


    public void hashPassword() {
        this.password = new BCryptPasswordEncoder().encode(this.password);
    }

    public boolean checkPassword(String password) {
        return new BCryptPasswordEncoder().matches(password, this.password);
    }

    public boolean isFinalUser() {
        return "FINAL".equals(this.rol);
    }

}
