package com.forcet.service.rest.model.batch.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@Slf4j
@Document(collection = "batch")
public class BatchResponse {

    @Id
    @ApiModelProperty(hidden = true)
    String id;
    @ApiModelProperty(position = 1, notes = "${batch.description.code}", required = true)
    String code;
    @ApiModelProperty(position = 2, notes = "${batch.description.userCode}", required = true)
    String userCode;
    @ApiModelProperty(position = 3, notes = "${batch.description.name}", required = true)
    String name;
    @ApiModelProperty(position = 4, notes = "${batch.description.description}", required = true)
    String description;
    @ApiModelProperty(position = 5, notes = "${batch.description.type}", required = true)
    String type;
    @ApiModelProperty(hidden = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    LocalDate registerDate;

}
