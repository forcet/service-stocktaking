package com.forcet.service.rest.repository.prime.bio.animal;

import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SpecieAnimalRepository extends MongoRepository<SpecieAnimalResponse, String> {


    @Query("{'userCode' : ?0 }")
    List<SpecieAnimalResponse> findAllSpecieAnimalByUser(String userCode);

    @Query("{'userCode' : ?0,  'animalId' : ?1}")
    Optional<SpecieAnimalResponse> findSpecieAnimalByUser(String userCode, String animalId);

}
