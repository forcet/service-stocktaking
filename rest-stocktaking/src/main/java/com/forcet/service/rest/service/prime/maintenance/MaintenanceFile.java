package com.forcet.service.rest.service.prime.maintenance;

import com.forcet.service.rest.model.batch.response.BatchResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.prime.maintenace.resquest.MaintenanceRequest;
import com.forcet.service.rest.repository.batch.BatchRepository;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.service.template.files.BuilderFilesData;
import com.forcet.service.rest.util.ServiceUtil;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MaintenanceFile extends BuilderFilesData {

    MaintenanceRepository maintenanceRepository;
    BatchRepository batchRepository;

    ServiceUtil serviceUtil = new ServiceUtil();


    public MaintenanceFile(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, MaintenanceRepository maintenanceRepository, BatchRepository batchRepository) {
        super.fileRepository = fileRepository;
        super.messageService = messageService;
        super.userRepository = userRepository;
        this.maintenanceRepository = maintenanceRepository;
        this.batchRepository = batchRepository;
    }

    public static MaintenanceFile create(FileRepository fileRepository, MessageService messageService, UserRepository userRepository, MaintenanceRepository maintenanceRepository, BatchRepository batchRepository) {
        return new MaintenanceFile(fileRepository, messageService, userRepository, maintenanceRepository, batchRepository);
    }

    @Override
    protected void validateProduct(String[] data, String code, List<String> errors) {
        String codeMaintenance = data[0];
        MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(code, codeMaintenance).orElse(null);
        if (maintenanceResponse == null) {
            MaintenanceRequest request = MaintenanceRequest.builder()
                    .maintenanceId(codeMaintenance)
                    .userCode(code)
                    .name(data[1])
                    .elaborateDate(data[2])
                    .expirationDate(data[3])
                    .cost(serviceUtil.getBigDecimal(data[4]))
                    .measurement(data[5])
                    .quantity(serviceUtil.getBigDecimal(data[6]))
                    .notifyUser(data[7])
                    .batchId(data[8])
                    .build();

            MaintenanceServiceImpl service = new MaintenanceServiceImpl();
            MaintenanceResponse response = service.getNewMaintenanceResponse(request);
            response.setBatchDescription(getBatchName(response.getUserCode(), response.getBatchId()));
            maintenanceRepository.save(response);
        } else {
            errors.add(String.format("Elemento ya registrado previamente %s", data[0]));
        }
    }

    private String getBatchName(String code, String user) {
        return batchRepository.findCategoriesByUser(code, user).orElse(BatchResponse.builder().build()).getName();
    }

    @Override
    protected String getTransaction() {
        return "Alimentos";
    }

    @Override
    protected int getSizeFile() {
        return 9;
    }
}
