package com.forcet.service.rest.repository.files;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.function.Supplier;
import java.util.stream.Stream;

public interface FileRepository {

    void init(String code);

    void save(MultipartFile file, String code);

    Resource load(String name, String code);

    void deleteAll(String code);

    Supplier<Stream<Path>> loadAll(String code);

    boolean deleteFile(String name, String code);


}
