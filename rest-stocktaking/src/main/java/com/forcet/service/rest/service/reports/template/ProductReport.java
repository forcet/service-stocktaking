package com.forcet.service.rest.service.reports.template;

import com.forcet.service.rest.model.product.response.ProductResponse;
import com.forcet.service.rest.model.report.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProductReport extends FillReports {

    List<ProductResponse> listField;

    public ProductReport(List<ProductResponse> listField) {
        this.listField = listField;
    }

    public static ProductReport create(List<ProductResponse> listField) {
        return new ProductReport(listField);
    }

    @Override
    protected Field changeFieldReport() {
        List<ProductResponse> responseList = this.listField;
        List<List<Object>> rowList = responseList.stream().map(response -> {
            List<Object> row = new ArrayList<>();
            row.add(response.getProductId());
            row.add(response.getName());
            row.add(response.getMarketPrice());
            row.add(response.getRealPrice());
            row.add(response.getSellQuantity());
            row.add(response.getRejection());
            row.add(response.getRealGreed());
            row.add(response.getEstimateGreed());
            return row;
        }).collect(Collectors.toList());
        return Field.builder()
                .columnName(Arrays.asList("Código","Nombre", "Precio Mercado", "Precio Real", "Cantidad vendida", "Producto Rechazado", "Ganancia Real", "Ganancia Estimada"))
                .valueRows(rowList)
                .build() ;
    }
}
