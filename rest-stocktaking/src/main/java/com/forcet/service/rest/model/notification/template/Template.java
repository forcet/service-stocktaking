package com.forcet.service.rest.model.notification.template;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Template {

    String name;
    String email;
    String phone;
    List<Parameter> parameters;
    String message;
    String body;

}
