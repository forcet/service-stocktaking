package com.forcet.service.rest.repository.prime.maintenance;

import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MaintenanceRepository extends MongoRepository<MaintenanceResponse, String> {

    @Query("{'userCode' : ?0 }")
    List<MaintenanceResponse> findAllMaintenanceByUser(String userCode);

    @Query("{'userCode' : ?0,  'maintenanceId' : ?1}")
    Optional<MaintenanceResponse> findMaintenanceByUser(String userCode, String maintenanceId);

    @Query("{'userCode' : ?0,  'batchId' : ?1}")
    List<MaintenanceResponse> findAllMaintenanceByUserBatch(String userCode, String batchId);

}
