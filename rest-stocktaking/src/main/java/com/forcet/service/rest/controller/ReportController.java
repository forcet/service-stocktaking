package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.report.chart.InfoChart;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.reports.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("reports")
@Api(value = "Generaci\u00F3n de Reportes", description = "API generar reportes", tags = {"Reportes"})
public class ReportController {

    @Autowired
    ReportService reportService;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/report", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${report.description.excel}")
    public ResponseEntity<Resource> getReport(@RequestParam("code") String code, @RequestParam("table") String table) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=reporte.xlsx")
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .cacheControl(CacheControl.noCache())
                .body(reportService.getReport(code, table));
    }

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/chart", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${report.description.chart}")
    public ResponseEntity<Summary<InfoChart>> getChartData(@RequestParam("code") String code, @RequestParam String table) {
        return reportService.getCharData(code, table);
    }


}
