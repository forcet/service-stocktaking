package com.forcet.service.rest.model.prime.maintenace.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "maintenance")
public class MaintenanceResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${maintenance.description.id}")
    String id;

    @ApiModelProperty(position = 2, notes = "${maintenance.description.maintenanceId}")
    String maintenanceId;

    @ApiModelProperty(position = 3, notes = "${maintenance.description.userCode}")
    String userCode;

    @ApiModelProperty(position = 4, notes = "${maintenance.description.name}")
    String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 5, notes = "${maintenance.description.elaborateDate}")
    String elaborateDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 6, notes = "${maintenance.description.expirationDate}")
    String expirationDate;

    @ApiModelProperty(position = 7, notes = "${maintenance.description.cost}")
    BigDecimal cost;

    @ApiModelProperty(position = 8, notes = "${maintenance.description.measurement}")
    String measurement;

    @ApiModelProperty(position = 9, notes = "${maintenance.description.quantity}")
    BigDecimal quantity;

    @ApiModelProperty(position = 10, notes = "${maintenance.description.lostQuantity}")
    BigDecimal lostQuantity;

    @ApiModelProperty(position = 11, notes = "${maintenance.description.age}")
    BigDecimal age; //

    @ApiModelProperty(position = 12, notes = "${maintenance.description.duration}")
    BigDecimal  duration; // elaboracion - expiracion

    @ApiModelProperty(position = 13, notes = "${maintenance.description.alertDuration}")
    boolean alertDuration; //age y duracion //Alerta de duracion cuando tiene cierta edad validar en tercios

    @ApiModelProperty(position = 14, notes = "${maintenance.description.alertQuantity}")
    boolean alertQuantity;// Alerta de cantidad

    @ApiModelProperty(position = 15, notes = "${maintenance.description.quantityActual}")
    BigDecimal quantityActual; //Resta en base al consumo

    @ApiModelProperty(position = 16, notes = "${maintenance.description.batchId}")
    String batchId;

    @ApiModelProperty(position = 17, notes = "${maintenance.description.batchDescription}")
    String batchDescription;

    @ApiModelProperty(position = 18, notes = "${maintenance.description.notifyUser}")
    String notifyUser ; // Cantidad perdida

    @ApiModelProperty(position = 19, notes = "${maintenance.description.isActive}")
    String isActive;

    @ApiModelProperty(position = 20, notes = "${maintenance.description.messageLose}")
    List<String> messageLose;


}
