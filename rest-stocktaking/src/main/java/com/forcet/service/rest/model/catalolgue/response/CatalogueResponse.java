package com.forcet.service.rest.model.catalolgue.response;


import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Builder
@Document(collection = "catalogue")
public class CatalogueResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${scheduler.description.id}")
    private String id;
    @ApiModelProperty(position = 2, notes = "${catalogue.description.code}")
    private String code;
    @ApiModelProperty(position = 3, notes = "${catalogue.description.value}")
    private String value;
}
