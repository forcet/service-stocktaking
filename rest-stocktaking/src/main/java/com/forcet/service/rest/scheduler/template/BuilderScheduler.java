package com.forcet.service.rest.scheduler.template;

import com.forcet.service.rest.model.notification.mail.Email;
import com.forcet.service.rest.model.notification.phone.Phone;
import com.forcet.service.rest.model.notification.template.Template;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.user.Person;
import com.forcet.service.rest.model.user.response.UserResponse;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Slf4j
public abstract class BuilderScheduler {

    MessageService messageService;

    MaintenanceRepository maintenanceRepository;

    ServiceUtil serviceUtil = new ServiceUtil();


    public void updateTask(MaintenanceResponse response, UserResponse userResponse) {
        BigDecimal result = getSubtractQuality(response.getQuantityActual());
        LocalDate elaborateDate = serviceUtil.getLocalDate(response.getElaborateDate());
        long days = serviceUtil.getDaysCountBetweenDates(elaborateDate, LocalDate.now());
        boolean isSendMessage = sendNotification(result, response.getQuantity());
        boolean isNotifyUser = response.getNotifyUser() != null && "1".equals(response.getNotifyUser()) ? Boolean.TRUE : Boolean.FALSE;
        response.setAge(new BigDecimal(days));
        response.setAlertQuantity(isSendMessage);
        if (isSendMessage ) {
            String message = String.format("Estimado %s. La cantidad de " +
                            "de %s - [ %s ], se esta agotando. %s pronto quedara sin alimento", userResponse.getPerson().getNames(),
                    response.getName(), response.getQuantityActual(), getSpecieName());
            log.info(message);
            Template template = getTemplate(response, userResponse, getSpecieName());
            sendMessage(template, isNotifyUser);
        } else {
            response.setQuantityActual(result);
        }
        maintenanceRepository.save(response);
    }



    private void sendMessage(Template template, boolean isNotifyUser) {
        if (isNotifyUser ) {
            Email email = Email.builder().body(template.getMessage()).sender(template.getEmail()).affair("Alimento en porcentajes bajos").build();
            Phone phone = Phone.builder().codeCountry("593").phoneNumber(template.getPhone()).message(template.getMessage()).build();
            messageService.sendMessageEmail(email);
            messageService.sendMessagePhone(phone);
        }
    }

    private Template getTemplate(MaintenanceResponse response, UserResponse userResponse, String name) {
        Person person = userResponse.getPerson();
        String message = String.format("Estimado %s. La cantidad de " +
                        "de %s - [ %s ], se esta agotando. %s pronto quedara sin alimento", userResponse.getPerson().getNames(),
                response.getName(), response.getQuantityActual(), name);
        return Template.builder().name(person.getNames()).email(person.getEmail()).phone(person.getPhone()).message(message).build();
    }

    private boolean sendNotification(BigDecimal actual, BigDecimal old) {
        BigDecimal percentage = new BigDecimal(100);
        BigDecimal result = actual.multiply(percentage).divide(old, RoundingMode.HALF_UP);
        return (BigDecimal.TEN.compareTo(result) > 0);
    }

    protected abstract BigDecimal getSubtractQuality(BigDecimal qualityActual);

    protected abstract String getSpecieName();


}
