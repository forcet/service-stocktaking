package com.forcet.service.rest.service.reports;

import com.forcet.service.rest.model.report.chart.InfoChart;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

public interface ReportService {

    Resource getReport(String code, String tabla);

    ResponseEntity<Summary<InfoChart>> getCharData(String code, String tabla);
}
