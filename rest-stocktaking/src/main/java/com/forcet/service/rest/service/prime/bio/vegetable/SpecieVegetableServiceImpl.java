package com.forcet.service.rest.service.prime.bio.vegetable;

import com.forcet.service.rest.model.prime.bio.vegetable.request.SpecieVegetableRequest;
import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.prime.bio.vegetable.SpecieVegetableRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class SpecieVegetableServiceImpl implements SpecieVegetableService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    private static final String ERROR_NO_EXIST = "No se encontro el elemento buscado";

    @Autowired
    SpecieVegetableRepository specieVegetableRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    MessageService messageService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MaintenanceRepository maintenanceRepository;


    @Override
    public ResponseEntity<Summary<SpecieVegetableResponse>> saveSpecieVegetable(SpecieVegetableRequest request) {
        Summary<SpecieVegetableResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieVegetableResponse>> responseEntity;
        try {
            SpecieVegetableResponse specieVegetableResponse = specieVegetableRepository.findSpecieVegetableByUser(request.getUserCode(), request.getVegetableId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("El elemento no existe"),
                    "El elemento ingresado ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (specieVegetableResponse == null ) {
                SpecieVegetableResponse response = specieVegetableRepository.save(getNewSpecieVegetableResponse(request));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieVegetableResponse>> updateSpecieVegetable(SpecieVegetableRequest request) {
        Summary<SpecieVegetableResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieVegetableResponse>> responseEntity;
        try {
            SpecieVegetableResponse specieVegetableResponse = specieVegetableRepository.findSpecieVegetableByUser(request.getUserCode(), request.getVegetableId()).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun elemento"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (specieVegetableResponse != null) {
                SpecieVegetableResponse updateSpecieVegetableRequest = specieVegetableRepository.save(getUpdateSpecieVegetableResponse(request, specieVegetableResponse));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(updateSpecieVegetableRequest), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    private String dateFormat(String date) {
        ServiceUtil serviceUtil = new ServiceUtil();
        LocalDate localDate = serviceUtil.getLocalDate(date);
        return serviceUtil.getStringDate(localDate);
    }

    private String getMaintenanceDescription(String code, String codeMaintenance) {
        if (Optional.ofNullable(maintenanceRepository).isPresent()) {
            MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(code, codeMaintenance).orElse(null);
            if (maintenanceResponse != null) {
                return maintenanceResponse.getName();
            }
        }
        return null;
    }

    public SpecieVegetableResponse getNewSpecieVegetableResponse(SpecieVegetableRequest request) {
        return SpecieVegetableResponse
                .builder()
                .vegetableId(request.getVegetableId())
                .userCode(request.getUserCode())
                .name(request.getName())
                .buyDate(dateFormat(request.getBuyDate()))
                .codeMaintenance(request.getCodeMaintenance())
                .codeMaintenanceDescription(getMaintenanceDescription(request.getUserCode(), request.getCodeMaintenance()))
                .cost(request.getCost())
                .total(request.getTotal())
                .diaryConsume(request.getDiaryConsume())
                .notifyUser(request.getNotifyUser())
                .build();
    }

    public SpecieVegetableResponse getUpdateSpecieVegetableResponse(SpecieVegetableRequest request, SpecieVegetableResponse response) {
        return SpecieVegetableResponse
                .builder()
                .id(response.getId())
                .vegetableId(response.getVegetableId())
                .userCode(response.getUserCode())
                .name(request.getName())
                .buyDate(dateFormat(request.getBuyDate()))
                .codeMaintenance(request.getCodeMaintenance())
                .codeMaintenanceDescription(getMaintenanceDescription(request.getUserCode(), request.getCodeMaintenance()))
                .cost(request.getCost())
                .total(request.getTotal())
                .diaryConsume(request.getDiaryConsume())
                .notifyUser(request.getNotifyUser())
                .build();
    }

    @Override
    public ResponseEntity<Summary<SpecieVegetableResponse>> deleteSpecieVegetable(String vegetableId) {
        Summary<SpecieVegetableResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieVegetableResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el elemento buscado"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            SpecieVegetableResponse specieVegetableResponse = specieVegetableRepository.findById(vegetableId).orElse(null);
            if (specieVegetableResponse != null) {
                specieVegetableRepository.delete(specieVegetableResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieVegetableResponse>> deleteAllSpecieVegetable(String code) {
        Summary<SpecieVegetableResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieVegetableResponse>> responseEntity;
        try {
            List<SpecieVegetableResponse> responses = specieVegetableRepository.findAllSpecieVegetableByUser(code);
            specieVegetableRepository.deleteAll(responses);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieVegetableResponse>> findSpecieVegetableById(String vegetableId) {
        Summary<SpecieVegetableResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieVegetableResponse>> responseEntity;
        try {
            SpecieVegetableResponse specieVegetableResponse = specieVegetableRepository.findById(vegetableId).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el elemento"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (specieVegetableResponse != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(specieVegetableResponse), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieVegetableResponse>> findAllSpecieVegetableByUser(String userId) {
        Summary<SpecieVegetableResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieVegetableResponse>> responseEntity;
        try {
            List<SpecieVegetableResponse> responses = specieVegetableRepository.findAllSpecieVegetableByUser(userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(responses), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<List<String>>> saveSpecieVegetableFile(MultipartFile[] files, String userId) {
        ResponseEntity<Summary<List<String>>> responseEntity;
        Summary<List<String>> summary = new Summary<>();
        try {
            List<String> resultErrors = VegetableFile.create(fileRepository, messageService, userRepository, specieVegetableRepository, maintenanceRepository)
                    .saveUserFiles(files, userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(resultErrors), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}
