package com.forcet.service.rest.service.user;

import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.user.request.UserRequest;
import com.forcet.service.rest.model.user.response.UserResponse;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<Summary<UserResponse>> saveUser(UserRequest userRequest);

    ResponseEntity<Summary<UserResponse>> updateUser(UserRequest userRequest);

    ResponseEntity<Summary<UserResponse>> deleteUser(String userId);

    ResponseEntity<Summary<UserResponse>> findById(String userName);

    ResponseEntity<Summary<UserResponse>> findAll();


}
