package com.forcet.service.rest.error;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class ExceptionFilter extends OncePerRequestFilter {

    private static final String JWT_EXPIRED = "error.jwtExpired";
    private static final String GENERIC_ERROR = "error.generic";

    private MessageSource messageSource;

    public ExceptionFilter(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        try {
            chain.doFilter(request, response);
        } catch (RuntimeException e) {
            Locale locale = LocaleContextHolder.getLocale();

            Error apiError;
            if (e instanceof ExpiredJwtException) {
                apiError = new Error(BAD_REQUEST);
                String errorMessage = messageSource.getMessage(JWT_EXPIRED, null, locale);
                apiError.setMessage(errorMessage);
                response.setStatus(HttpStatus.BAD_REQUEST.value());

            } else {
                apiError = new Error(INTERNAL_SERVER_ERROR);
                String errorMessage = messageSource.getMessage(GENERIC_ERROR, null, locale);
                apiError.setMessage(errorMessage);
                response.setStatus(INTERNAL_SERVER_ERROR.value());
            }


            response.getWriter().write(convertObjectToJson(apiError));
        }
    }

    public String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}
