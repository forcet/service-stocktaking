package com.forcet.service.rest.service.scheduler;

import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.scheduler.request.SchedulerRequest;
import com.forcet.service.rest.model.scheduler.response.SchedulerResponse;
import org.springframework.http.ResponseEntity;

public interface SchedulerService {

    ResponseEntity<Summary<SchedulerResponse>> saveScheduling(SchedulerRequest request);

    ResponseEntity<Summary<SchedulerResponse>> updateScheduling(SchedulerRequest request);

    ResponseEntity<Summary<SchedulerResponse>> deleteScheduling(String code);

    ResponseEntity<Summary<SchedulerResponse>> findByUserAllScheduling(String code);



}
