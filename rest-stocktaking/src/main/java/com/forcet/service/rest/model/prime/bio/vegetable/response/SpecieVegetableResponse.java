package com.forcet.service.rest.model.prime.bio.vegetable.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
@Setter
@Document(collection = "specieVegetable")
public class SpecieVegetableResponse {

    @Id
    @ApiModelProperty(position = 1, notes = "${vegetable.description.id}")
    String id;

    @ApiModelProperty(position = 2, notes = "${vegetable.description.code}")
    String vegetableId;

    @ApiModelProperty(position = 3, notes = "${vegetable.description.userCode}")
    String userCode;

    @ApiModelProperty(position = 4, notes = "${vegetable.description.name}")
    String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    @ApiModelProperty(position = 5, notes = "${vegetable.description.buyDate}")
    String buyDate;

    @ApiModelProperty(position = 6, notes = "${vegetable.description.codeMaintenance}")
    String codeMaintenance;

    @ApiModelProperty(position = 7, notes = "${vegetable.description.codeMaintenance}")
    String codeMaintenanceDescription;

    @ApiModelProperty(position = 8, notes = "${vegetable.description.cost}")
    BigDecimal cost;

    @ApiModelProperty(position = 9, notes = "${vegetable.description.total}")
    BigDecimal total;

    @ApiModelProperty(position = 10, notes = "${vegetable.description.diaryConsume}")
    BigDecimal diaryConsume;

    @ApiModelProperty(position = 11, notes = "${vegetable.description.loseSpecie}")
    BigDecimal loseSpecie;

    @ApiModelProperty(position = 12, notes = "${vegetable.description.messageLose}")
    List<String> messageLose;

    @ApiModelProperty(position = 13, notes = "${vegetable.description.notifyUser}")
    String notifyUser;

}
