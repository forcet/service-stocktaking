package com.forcet.service.rest.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
@Api(value = "Validaci\u00F3n Login", description = "API para validar el login de Usuarios", tags = {"Login"})
public class AuthController {
}
