package com.forcet.service.rest.service.product;

import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.product.request.ProductRequest;
import com.forcet.service.rest.model.product.response.ProductResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface ProductService {

    ResponseEntity<Summary<ProductResponse>> saveProduct(ProductRequest productRequest);

    ResponseEntity<Summary<ProductResponse>> updateProduct(ProductRequest productRequest);

    ResponseEntity<Summary<ProductResponse>> deleteProduct(String productId);

    ResponseEntity<Summary<ProductResponse>> deleteAllProduct(String code);

    ResponseEntity<Summary<ProductResponse>> findProductById(String productId);

    ResponseEntity<Summary<ProductResponse>> findAllProductByUser(String userId);

    ResponseEntity<Summary<List<String>>> saveFile(MultipartFile[] files, String userId);





}
