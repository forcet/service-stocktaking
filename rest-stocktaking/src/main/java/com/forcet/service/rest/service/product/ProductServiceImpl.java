package com.forcet.service.rest.service.product;


import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.product.request.ProductRequest;
import com.forcet.service.rest.model.product.response.ProductResponse;
import com.forcet.service.rest.repository.batch.BatchRepository;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.product.ProductRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;


@Slf4j
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    private static final String ERROR_NO_EXIST = "No se encontro el producto buscado";

    @Autowired
    ProductRepository productRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    BatchRepository batchRepository;

    @Autowired
    MessageService messageService;

    @Autowired
    UserRepository userRepository;

    ServiceUtil serviceUtil = new ServiceUtil();

    @Override
    public ResponseEntity<Summary<ProductResponse>> saveProduct(ProductRequest productRequest) {
        Summary<ProductResponse> summary = new Summary<>();
        ResponseEntity<Summary<ProductResponse>> responseEntity;

        try {
            ProductResponse previewProduct = productRepository.findProductByUser(productRequest.getUsername(), productRequest.getProductId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("El producto existe"),
                    "El producto ingresado ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (previewProduct == null ) {
                ProductResponse newProduct = productRepository.save(getNewProductResponse(productRequest));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(newProduct), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<ProductResponse>> updateProduct(ProductRequest productRequest) {
        Summary<ProductResponse> summary = new Summary<>();
        ResponseEntity<Summary<ProductResponse>> responseEntity;
        try {
            ProductResponse product = productRepository.findProductByUser(productRequest.getUsername(), productRequest.getProductId()).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun producto"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (product != null) {
                ProductResponse updateProduct = productRepository.save(getUpdateProductResponse(productRequest, product));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(updateProduct), HttpStatus.OK);
            }
        }catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<ProductResponse>> deleteProduct(String productId) {

        Summary<ProductResponse> summary = new Summary<>();
        ResponseEntity<Summary<ProductResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el producto"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            ProductResponse product = productRepository.findById(productId).orElse(null);
            if (product != null) {
                productRepository.delete(product);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<ProductResponse>> deleteAllProduct(String code) {
        Summary<ProductResponse> summary = new Summary<>();
        ResponseEntity<Summary<ProductResponse>> responseEntity;
        try {
            List<ProductResponse> responses = productRepository.findAllProductByUser(code);
            productRepository.deleteAll(responses);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<ProductResponse>> findProductById(String productId) {
        Summary<ProductResponse> summary = new Summary<>();
        ResponseEntity<Summary<ProductResponse>> responseEntity;
        try {
            ProductResponse product = productRepository.findById(productId).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el producto"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (product != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(product), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;

    }


    @Override
    public ResponseEntity<Summary<ProductResponse>> findAllProductByUser(String userId) {
        ResponseEntity<Summary<ProductResponse>> responseEntity;
        Summary<ProductResponse> summary = new Summary<>();
        try {
            List<ProductResponse> product = productRepository.findAllProductByUser(userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(product), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<List<String>>> saveFile(MultipartFile[] files, String userId) {
        ResponseEntity<Summary<List<String>>> responseEntity;
        Summary<List<String>> summary = new Summary<>();

        try {
            List<String> resultErrors = ProductFile.create(fileRepository, messageService, userRepository, productRepository).saveUserFiles(files, userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(resultErrors), HttpStatus.OK);

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    public  ProductResponse getNewProductResponse(ProductRequest request) {
        LocalDate releaseDate = serviceUtil.getLocalDate(request.getReleaseDate());
        long days = serviceUtil.getDaysCountBetweenDates(releaseDate, LocalDate.now());
        return ProductResponse.builder()
                .productId(request.getProductId())
                .username(request.getUsername())
                .name(request.getName())
                .realPrice(request.getRealPrice())
                .marketPrice(request.getMarketPrice())
                .quantity(request.getQuantity())
                .sellQuantity(request.getSellQuantity())
                .releaseDate(dateFormat(request.getReleaseDate()))
                .rejection(serviceUtil.getSubtractDecimal(request.getQuantity(), request.getSellQuantity()))
                .realGreed(serviceUtil.getAddDecimal(request.getRealPrice(), request.getSellQuantity()))
                .estimateGreed(serviceUtil.getAddDecimal(request.getMarketPrice(), request.getQuantity()))
                .antiquity(new BigDecimal(days))
                .build();
    }

    private ProductResponse getUpdateProductResponse(ProductRequest request, ProductResponse response) {
        LocalDate releaseDate = serviceUtil.getLocalDate(request.getReleaseDate());
        long days = serviceUtil.getDaysCountBetweenDates(releaseDate, LocalDate.now());
        return ProductResponse.builder()
                .id(response.getId())
                .productId(response.getProductId())
                .username(response.getUsername())
                .realPrice(request.getRealPrice())
                .name(request.getName())
                .marketPrice(request.getMarketPrice())
                .quantity(request.getQuantity())
                .sellQuantity(request.getSellQuantity())
                .releaseDate(dateFormat(request.getReleaseDate()))
                .rejection(serviceUtil.getSubtractDecimal(request.getQuantity(), request.getSellQuantity()))
                .realGreed(serviceUtil.getAddDecimal(request.getRealPrice(), request.getSellQuantity()))
                .estimateGreed(serviceUtil.getAddDecimal(request.getMarketPrice(), request.getQuantity()))
                .antiquity(new BigDecimal(days))
                .build();
    }


    private String dateFormat(String date) {
        LocalDate localDate = serviceUtil.getLocalDate(date);
        return serviceUtil.getStringDate(localDate);
    }

}
