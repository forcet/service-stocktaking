package com.forcet.service.rest.repository.files;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Slf4j
@Repository
public class FileRepositoryImpl implements FileRepository {

    private final Path root = Paths.get("uploads");

    @Override
    public void init(String code) {
        try {
            Path newPath = this.root.resolve(code);
            Files.createDirectories(newPath);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new RuntimeException("Error iniciar el storage");
        }
    }

    @Override
    public void save(MultipartFile file, String code) {
        try {
            Path newPath = this.root.resolve(code);
            Files.copy(file.getInputStream(), newPath.resolve(Objects.requireNonNull(file.getOriginalFilename())));
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new RuntimeException("No se puede generar el archivo");
        }
    }

    @Override
    public Resource load(String name, String code) {
        try {
            Path file = this.root.resolve(code).resolve(name);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("No se puede leer el archivo");
            }
        } catch (MalformedURLException e) {
            log.error(e.getLocalizedMessage(), e);
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll(String code) {
        Path newPath = this.root.resolve(code);
        FileSystemUtils.deleteRecursively(newPath.toFile());
    }

    @Override
    public Supplier<Stream<Path>> loadAll(String code) {
        Path newPath = this.root.resolve(code);
        try {
            return () -> {
                try {
                    return Files.walk(newPath, 1).filter(path -> !path.equals(newPath)).map(newPath::relativize);
                } catch (IOException e) {
                    log.error(e.getLocalizedMessage(), e);
                    throw new RuntimeException("No se puede generar cargar los archivos");
                }
            };
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new RuntimeException("No se puede generar cargar los archivos");
        }
    }

    @Override
    public boolean deleteFile(String name, String code) {
        boolean isDelete = Boolean.FALSE;
        try {
            isDelete = Files.deleteIfExists(this.root.resolve(code).resolve(name));
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return isDelete;
    }
}
