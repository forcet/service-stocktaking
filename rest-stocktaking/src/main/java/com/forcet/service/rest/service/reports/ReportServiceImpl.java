package com.forcet.service.rest.service.reports;

import com.forcet.service.rest.model.fixed.response.FixedResponse;
import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.recap.Recap;
import com.forcet.service.rest.model.report.Field;
import com.forcet.service.rest.model.report.chart.Data;
import com.forcet.service.rest.model.report.chart.InfoChart;
import com.forcet.service.rest.model.report.chart.model.Component;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.model.product.response.ProductResponse;
import com.forcet.service.rest.repository.fixed.FixedRepository;
import com.forcet.service.rest.repository.prime.bio.animal.SpecieAnimalRepository;
import com.forcet.service.rest.repository.prime.bio.vegetable.SpecieVegetableRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.product.ProductRepository;
import com.forcet.service.rest.service.recap.RecapService;
import com.forcet.service.rest.service.reports.template.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    MaintenanceRepository maintenanceRepository;

    @Autowired
    SpecieAnimalRepository specieAnimalRepository;

    @Autowired
    SpecieVegetableRepository specieVegetableRepository;

    @Autowired
    FixedRepository fixedRepository;

    @Autowired
    RecapService recapService;

    @Override
    public Resource getReport(String code, String table) {
        Field fieldReports;
        Component component = Component.fromCode(table);
        switch (component) {
            case PRODUCT:
                List<ProductResponse> productRequestList = productRepository.findAllProductByUser(code);
                fieldReports = ProductReport.create(productRequestList).getField();
                break;
            case FIXED:
                List<FixedResponse> fixedResponseList = fixedRepository.findAllFixedByUser(code);
                fieldReports = FixedReport.create(fixedResponseList).getField();
                break;
            case MAINTENANCE:
                List<MaintenanceResponse> maintenanceResponseList = maintenanceRepository.findAllMaintenanceByUser(code);
                fieldReports = MaintenanceReport.create(maintenanceResponseList).getField();
                break;
            case SPECIE_ANIMAL:
                List<SpecieAnimalResponse> specieAnimalResponseList = specieAnimalRepository.findAllSpecieAnimalByUser(code);
                fieldReports = AnimalReport.create(specieAnimalResponseList).getField();
                break;
            case SPECIE_VEGETABLE:
                List<SpecieVegetableResponse> specieVegetableResponseList = specieVegetableRepository.findAllSpecieVegetableByUser(code);
                fieldReports = VegetableReport.create(specieVegetableResponseList).getField();
                break;
            default:
                fieldReports = Field.builder().build();
        }
        return new ByteArrayResource(createXSLFile(fieldReports, code));
    }

    private byte[] createXSLFile(Field fieldReports, String userId) {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();

        try (SXSSFWorkbook workbook = new SXSSFWorkbook(1000)) {
            Sheet sheet = workbook.createSheet(userId);
            AtomicInteger rowNum = new AtomicInteger(1);
            Row headerRow = sheet.createRow(rowNum.get());
            AtomicInteger cellHeaderNum = new AtomicInteger();
            fieldReports.getColumnName().forEach(header -> {
                cellHeaderNum.getAndIncrement();
                headerRow.createCell(cellHeaderNum.get()).setCellValue(header);
                CellStyle cellStyle = workbook.createCellStyle();
                cellStyle.setFillBackgroundColor(IndexedColors.LIGHT_BLUE.getIndex());
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                Font font = workbook.createFont();
                font.setColor(IndexedColors.WHITE.getIndex());
                cellStyle.setFont(font);
                headerRow.getCell(cellHeaderNum.get()).setCellStyle(cellStyle);
            });

            fieldReports.getValueRows().forEach(listRow -> {
                rowNum.getAndIncrement();
                AtomicInteger cellRowNum = new AtomicInteger();
                Row currentRow = sheet.createRow(rowNum.get());
                listRow.forEach(row -> {
                    cellRowNum.getAndIncrement();
                    currentRow.createCell(cellRowNum.get()).setCellValue(row.toString());
                    CellStyle cellStyle = workbook.createCellStyle();
                    cellStyle.setWrapText(Boolean.TRUE);
                    currentRow.getCell(cellRowNum.get()).setCellStyle(cellStyle);
                });

            });

            workbook.write(byteArray);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }

        return byteArray.toByteArray();

    }


    @Override
    public ResponseEntity<Summary<InfoChart>> getCharData(String code, String table) {
        Summary<InfoChart> summary = new Summary<>();
        ResponseEntity<Summary<InfoChart>> responseEntity;
        try {
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(getDataCharts(code, table)), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    "Ha ocurrido un error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    private InfoChart getDataCharts(String code, String table) {
        Component component = Component.fromCode(table);
        List<Data> listData;
        List<String> colors;
        switch (component) {
            case PRODUCT:
                List<ProductResponse> productRequestList = productRepository.findAllProductByUser(code);
                listData = productRequestList.stream()
                        .map(product -> Data.builder()
                                .name(product.getName())
                                .value(product.getRealGreed())
                                .build())
                        .collect(Collectors.toList());
                colors = productRequestList.stream().map(product -> getRandom()).collect(Collectors.toList());

                break;
            case MAINTENANCE:
                List<MaintenanceResponse> maintenanceResponseList = maintenanceRepository.findAllMaintenanceByUser(code);
                listData = maintenanceResponseList.stream()
                        .map(maintenance -> Data.builder()
                                .name(maintenance.getName())
                                .value(maintenance.getQuantityActual()).build())
                        .collect(Collectors.toList());
                colors = maintenanceResponseList.stream().map(maintenance -> getRandom()).collect(Collectors.toList());
                break;
            case SPECIE_VEGETABLE:
                List<SpecieVegetableResponse> specieVegetableResponseList = specieVegetableRepository.findAllSpecieVegetableByUser(code);
                listData = specieVegetableResponseList.stream()
                        .map(specieVegetable -> Data.builder()
                                .name(specieVegetable.getName())
                                .value(specieVegetable.getTotal()).build())
                        .collect(Collectors.toList());
                colors = specieVegetableResponseList.stream().map(specieVegetable -> getRandom()).collect(Collectors.toList());
                break;
            case SPECIE_ANIMAL:
                List<SpecieAnimalResponse> specieAnimalResponseList = specieAnimalRepository.findAllSpecieAnimalByUser(code);
                listData = specieAnimalResponseList.stream()
                        .map(specialAnimal -> Data.builder()
                                .name(specialAnimal.getName())
                                .value(specialAnimal.getDiaryConsume()).build())
                        .collect(Collectors.toList());
                colors = specieAnimalResponseList.stream().map(specieAnimal -> getRandom()).collect(Collectors.toList());
                break;
            case FIXED:
                List<FixedResponse> fixedResponseList = fixedRepository.findAllFixedByUser(code);
                listData = fixedResponseList.stream()
                        .map(fixed -> Data.builder()
                                .name(fixed.getName())
                                .value(fixed.getCost()).build()).collect(Collectors.toList());
                colors = fixedResponseList.stream().map(fixed -> getRandom()).collect(Collectors.toList());
                break;
            case RESULT:
                ResponseEntity<Summary<List<Recap>>> responseEntity = recapService.findRecapByUser(code);
                List<Recap> recapList = Objects.requireNonNull(responseEntity.getBody()).getBody();
                listData = recapList.stream().map(recap -> Data.builder()
                        .name(recap.getName())
                        .value(recap.getQualityActual())
                        .build()).collect(Collectors.toList());
                colors = recapList.stream().map(recap -> getRandom()).collect(Collectors.toList());
                break;

            default:
                listData = new ArrayList<>();
                colors = new ArrayList<>();
        }
        return InfoChart.builder().data(listData).colors(colors).build();
    }

    private String getRandom() {
        Random rand = new SecureRandom();
        List<String> givenList = Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
        StringBuilder value = new StringBuilder("#");
        for (int i = 0; i < 6; i++) {
            int randomIndex = rand.nextInt(givenList.size());
            String randomElement = givenList.get(randomIndex);
            value.append(String.join("", randomElement));
        }
        return value.toString();
    }

}
