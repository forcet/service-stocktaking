package com.forcet.service.rest.service.recap;


import com.forcet.service.rest.model.recap.Recap;
import com.forcet.service.rest.model.response.Summary;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface RecapService {

    ResponseEntity<Summary<List<Recap>>> findRecapByUser(String userId);

}
