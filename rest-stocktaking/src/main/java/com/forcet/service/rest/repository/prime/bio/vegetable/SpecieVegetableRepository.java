package com.forcet.service.rest.repository.prime.bio.vegetable;

import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SpecieVegetableRepository extends MongoRepository<SpecieVegetableResponse, String> {

    @Query("{'userCode' : ?0 }")
    List<SpecieVegetableResponse> findAllSpecieVegetableByUser(String userCode);

    @Query("{'userCode' : ?0,  'vegetableId' : ?1}")
    Optional<SpecieVegetableResponse> findSpecieVegetableByUser(String userCode, String vegetableId);
}
