package com.forcet.service.rest.service.reports.template;

import com.forcet.service.rest.model.report.Field;


public abstract class FillReports {

    protected abstract Field changeFieldReport();

    public Field getField() {
        return changeFieldReport();
    }
}
