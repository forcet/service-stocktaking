package com.forcet.service.rest.controller;


import com.forcet.service.rest.model.recap.Recap;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.service.recap.RecapService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("recap")
@Api(value = "Lista de resultados totales", description = "API que permite consultar los resultados para un usuario", tags = {"Resumen"})
public class RecapController {

    @Autowired
    RecapService service;

    @PreAuthorize("hasRole('ROLE_FINAL')")
    @GetMapping(value = "/findRecapByUser", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "${recap.findAll.description}")
    public ResponseEntity<Summary<List<Recap>>> findAllBatchByUser(@RequestParam("userCode") String userCode) {
        return service.findRecapByUser(userCode);
    }

}
