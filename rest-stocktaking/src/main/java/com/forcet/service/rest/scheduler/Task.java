package com.forcet.service.rest.scheduler;

import com.forcet.service.rest.model.notification.mail.Email;
import com.forcet.service.rest.model.notification.phone.Phone;
import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.model.scheduler.response.SchedulerResponse;
import com.forcet.service.rest.model.user.Person;
import com.forcet.service.rest.model.user.response.UserResponse;
import com.forcet.service.rest.repository.prime.bio.animal.SpecieAnimalRepository;
import com.forcet.service.rest.repository.prime.bio.vegetable.SpecieVegetableRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.scheduler.SchedulerRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.scheduler.template.TaskAnimal;
import com.forcet.service.rest.scheduler.template.TaskVegetable;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;


@Slf4j
@Component
public class Task {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SpecieVegetableRepository specieVegetableRepository;

    @Autowired
    SpecieAnimalRepository specieAnimalRepository;

    @Autowired
    MaintenanceRepository maintenanceRepository;

    @Autowired
    MessageService messageService;

    @Autowired
    SchedulerRepository schedulerRepository;

    ServiceUtil serviceUtil = new ServiceUtil();

    @Scheduled(cron = "0 0/30 7-10 * * *")
    public void run() {
        List<UserResponse> responseList = userRepository.findAll();
        responseList.forEach(user -> {
            if (user.isFinalUser()) {
                List<SpecieAnimalResponse> specieAnimalResponseList = specieAnimalRepository.findAllSpecieAnimalByUser(user.getUsername());
                specieAnimalResponseList.forEach(animal -> maintenanceRepository.findMaintenanceByUser(
                        user.getUsername(), animal.getCodeMaintenance()).ifPresent(
                                response ->  TaskAnimal.create(messageService, animal, maintenanceRepository).updateTask(response, user)));
                List<SpecieVegetableResponse> specieVegetableRequestList = specieVegetableRepository.findAllSpecieVegetableByUser(user.getUsername());
                specieVegetableRequestList.forEach(vegetable -> maintenanceRepository.findMaintenanceByUser(
                        user.getUsername(), vegetable.getCodeMaintenance()).ifPresent(
                                response -> TaskVegetable.create(messageService, vegetable, maintenanceRepository).updateTask(response, user)));
            }
            log.info(String.format("Current time is :: %s : user: %s", Calendar.getInstance().getTime(), user.getUsername()));
        });
    }



    @Scheduled(cron = "0 0/5 * * * ?") //"0 0/30 7-10 * * *"
    public void runParameterized(){
        List<UserResponse> responseList = userRepository.findAll();
        responseList.forEach(user -> {
            if (user.isFinalUser()) {
                List<SchedulerResponse> schedulerResponseList = schedulerRepository.findAllSchedulerByUser(user.getUsername());
                schedulerResponseList.forEach(schedulerResponse -> {
                    Person person = user.getPerson();
                    boolean isNotifyMail = getIsNotifyMail(schedulerResponse);
                    boolean isNotifyPhone = getIsNotifyPhone(schedulerResponse);
                    String message = String.format("Estimado, %s : Tiene un recordatorio sobre [ %s ], con nombre [ %s ]",
                            person.getNames(), schedulerResponse.getPersonalizeMessage(), schedulerResponse.getNameScheduler());
                    boolean isRangeDate = validDateRanges(schedulerResponse.getEndDate(), schedulerResponse.getStartDate());
                    if (isNotifyMail && isRangeDate) {
                        Email email = Email.builder().body(message)
                                .sender(person.getEmail())
                                .affair(String.format("%s - %s", schedulerResponse.getProductName(), schedulerResponse.getNameScheduler()))
                                .build();
                        messageService.sendMessageEmail(email);
                        log.info(message);
                    }

                    if (isNotifyPhone  && isRangeDate) {
                        Phone phone = Phone.builder()
                                .codeCountry("593")
                                .phoneNumber(person.getPhone())
                                .message(message).build();

                        messageService.sendMessagePhone(phone);
                        log.info(message);
                    }

                });
            }
        });
    }

    private boolean getIsNotifyMail(SchedulerResponse response) {
        return response.getNotNotifyMail() != null && "1".equals(response.getNotNotifyMail()) ? Boolean.FALSE : Boolean.TRUE;
    }

    private boolean getIsNotifyPhone(SchedulerResponse response) {
        return response.getNotNotifyPhone() != null &&  "1".equals(response.getNotNotifyPhone()) ? Boolean.FALSE : Boolean.TRUE;
    }

    private boolean validDateRanges(LocalDate max, LocalDate min) {
       LocalDate contain = LocalDate.now();
        if (max != null) {
            return serviceUtil.containsDateRanges(contain, max, min);
        }
        return min.isBefore(contain);

    }


}
