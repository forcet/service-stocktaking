package com.forcet.service.rest.scheduler.template;

import com.forcet.service.rest.model.prime.bio.vegetable.response.SpecieVegetableResponse;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.service.message.MessageService;

import java.math.BigDecimal;

public class TaskVegetable extends BuilderScheduler {

    SpecieVegetableResponse specieVegetableResponse;

    public TaskVegetable(MessageService messageService, SpecieVegetableResponse specieVegetableResponse, MaintenanceRepository maintenanceRepository) {
        super.messageService = messageService;
        super.maintenanceRepository = maintenanceRepository;
        this.specieVegetableResponse = specieVegetableResponse;
    }

    public static TaskVegetable create(MessageService messageService, SpecieVegetableResponse specieVegetableResponse, MaintenanceRepository maintenanceRepository) {
        return new TaskVegetable(messageService, specieVegetableResponse, maintenanceRepository);
    }


    @Override
    protected BigDecimal getSubtractQuality(BigDecimal qualityActual) {
        BigDecimal totalConsume = specieVegetableResponse.getTotal().multiply(specieVegetableResponse.getDiaryConsume());
        return qualityActual.subtract(totalConsume);
    }

    @Override
    protected String getSpecieName() {
        return specieVegetableResponse.getName();
    }

}
