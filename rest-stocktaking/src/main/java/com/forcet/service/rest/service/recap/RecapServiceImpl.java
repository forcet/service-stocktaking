package com.forcet.service.rest.service.recap;

import com.forcet.service.rest.model.batch.response.BatchResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.recap.Recap;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.repository.batch.BatchRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class RecapServiceImpl implements RecapService {

    @Autowired
    MaintenanceRepository maintenanceRepository;

    @Autowired
    BatchRepository batchRepository;

    ServiceUtil serviceUtil = new ServiceUtil();

    @Override
    public ResponseEntity<Summary<List<Recap>>> findRecapByUser(String userId) {
        Summary<List<Recap>> summary = new Summary<>();
        ResponseEntity<Summary<List<Recap>>> responseEntity;
        try {
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(getListRecap(userId)), HttpStatus.OK);
        }  catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    "Ocurrio un error interno", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }


    public List<Recap> getListRecap(String userId) {
        List<BatchResponse> batchRequestList = batchRepository.findAllCategoryByUser(userId);
        List<String>  codeList = batchRequestList.stream().filter(batch -> serviceUtil.containsDates(batch.getRegisterDate()))
                .map(BatchResponse::getCode).collect(Collectors.toList());
        return codeList.stream().map(code -> {
            List<MaintenanceResponse> maintenanceResponseList =  maintenanceRepository.findAllMaintenanceByUserBatch(userId, code);
            List<String> names = maintenanceResponseList.stream().map(MaintenanceResponse::getName).collect(Collectors.toList());
            String name = String.join(",", names);
            BigDecimal amounts = maintenanceResponseList
                    .stream()
                    .map(item -> item.getCost() == null ? BigDecimal.ZERO : item.getCost())
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal qualityActual = maintenanceResponseList
                    .stream()
                    .map(item -> item.getQuantityActual() == null ? BigDecimal.ZERO : item.getQuantityActual())
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal quality = maintenanceResponseList
                    .stream()
                    .map(item -> item.getQuantity() == null ? BigDecimal.ZERO : item.getQuantity())
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            return Recap.builder()
                    .name(name)
                    .cost(amounts)
                    .batchId(code)
                    .quality(quality)
                    .qualityActual(qualityActual)
                    .build();
        }).collect(Collectors.toList());
    }
}
