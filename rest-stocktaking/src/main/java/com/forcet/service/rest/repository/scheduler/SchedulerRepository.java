package com.forcet.service.rest.repository.scheduler;


import com.forcet.service.rest.model.scheduler.response.SchedulerResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SchedulerRepository extends MongoRepository<SchedulerResponse, String> {

    @Query("{'userCode' : ?0 }")
    List<SchedulerResponse> findAllSchedulerByUser(String username);

    @Query("{'userCode' : ?0,  'productId' : ?1}")
    Optional<SchedulerResponse> findSchedulerByUser(String username, String productId);

}
