package com.forcet.service.rest.service.fixed;


import com.forcet.service.rest.model.fixed.request.FixedRequest;
import com.forcet.service.rest.model.fixed.response.FixedResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.fixed.FixedRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@Transactional
public class FixedServiceImpl implements FixedService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    @Autowired
    FixedRepository fixedRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    MessageService messageService;

    @Autowired
    UserRepository userRepository;

    @Override
    public ResponseEntity<Summary<FixedResponse>> saveFixed(FixedRequest request) {
        Summary<FixedResponse> summary = new Summary<>();
        ResponseEntity<Summary<FixedResponse>> responseEntity;
        try {
            FixedResponse fixedResponse = fixedRepository.findFixedByUser(request.getUserCode(), request.getFixedId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("El elemento no existe"),
                    "El elemento ingresado ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (fixedResponse == null ) {
                FixedResponse response = fixedRepository.save(getNewFixedResponse(request));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<FixedResponse>> updateFixed(FixedRequest request) {
        Summary<FixedResponse> summary = new Summary<>();
        ResponseEntity<Summary<FixedResponse>> responseEntity;
        try {
            FixedResponse fixedResponse = fixedRepository.findFixedByUser(request.getUserCode(), request.getFixedId()).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun elemento"),
                    "No se encontro el elemento buscado", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (fixedResponse != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(getUpdateFixedResponse(request, fixedResponse)), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<FixedResponse>> deleteFixed(String fixedId) {
        Summary<FixedResponse> summary = new Summary<>();
        ResponseEntity<Summary<FixedResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el elemento"),
                    "No existe el elemento buscado", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            FixedResponse fixedResponse = fixedRepository.findById(fixedId).orElse(null);
            if (fixedResponse != null) {
                fixedRepository.delete(fixedResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<FixedResponse>> deleteAllFixed(String code) {
        Summary<FixedResponse> summary = new Summary<>();
        ResponseEntity<Summary<FixedResponse>> responseEntity;
        try {
            List<FixedResponse> responses = fixedRepository.findAllFixedByUser(code);
            fixedRepository.deleteAll(responses);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<FixedResponse>> findFixedById(String fixedId) {
        Summary<FixedResponse> summary = new Summary<>();
        ResponseEntity<Summary<FixedResponse>> responseEntity;
        try {
            FixedResponse fixedResponse = fixedRepository.findById(fixedId).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el elemento"),
                    "No se encontro el elemento buscado", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (fixedResponse != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(fixedResponse), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<FixedResponse>> findAllFixedByUser(String userId) {
        Summary<FixedResponse> summary = new Summary<>();
        ResponseEntity<Summary<FixedResponse>> responseEntity;
        try {
            List<FixedResponse> fixedResponseList = fixedRepository.findAllFixedByUser(userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(fixedResponseList), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<List<String>>> saveFixedFile(MultipartFile[] files, String userId) {
        ResponseEntity<Summary<List<String>>> responseEntity;
        Summary<List<String>> summary = new Summary<>();
        try {
            List<String> resultErrors = FixedFile.create(fileRepository, messageService, userRepository, fixedRepository).saveUserFiles(files, userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(resultErrors), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    public FixedResponse getNewFixedResponse(FixedRequest request) {
        return FixedResponse.builder()
                .fixedId(request.getFixedId())
                .userCode(request.getUserCode())
                .name(request.getName())
                .quantities(request.getQuantities())
                .status(request.getStatus())
                .cost(request.getCost())
                .build();
    }

    private FixedResponse getUpdateFixedResponse(FixedRequest request, FixedResponse response) {
        return FixedResponse.builder()
                .id(response.getId())
                .fixedId(response.getFixedId())
                .userCode(response.getUserCode())
                .name(request.getName())
                .quantities(request.getQuantities())
                .status(request.getStatus())
                .cost(request.getCost())
                .build();
    }
}
