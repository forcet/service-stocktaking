package com.forcet.service.rest.service.batch;

import com.forcet.service.rest.model.batch.request.BatchRequest;
import com.forcet.service.rest.model.batch.response.BatchResponse;
import com.forcet.service.rest.model.response.Summary;

import org.springframework.http.ResponseEntity;

public interface BatchService {

    ResponseEntity<Summary<BatchResponse>> saveBatch(BatchRequest batchRequest);

    ResponseEntity<Summary<BatchResponse>> updateBatch(BatchRequest batchRequest);

    ResponseEntity<Summary<BatchResponse>> deleteBatch(String code);

    ResponseEntity<Summary<BatchResponse>> deleteAllBatch(String userCode);

    ResponseEntity<Summary<BatchResponse>> findBatchById(String code);

    ResponseEntity<Summary<BatchResponse>> findAllBatchByUser(String userCode);

}
