package com.forcet.service.rest.service.prime.bio.animal;

import com.forcet.service.rest.model.prime.bio.animal.request.SpecieAnimalRequest;
import com.forcet.service.rest.model.prime.bio.animal.response.SpecieAnimalResponse;
import com.forcet.service.rest.model.prime.maintenace.response.MaintenanceResponse;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.repository.files.FileRepository;
import com.forcet.service.rest.repository.prime.bio.animal.SpecieAnimalRepository;
import com.forcet.service.rest.repository.prime.maintenance.MaintenanceRepository;
import com.forcet.service.rest.repository.user.UserRepository;
import com.forcet.service.rest.service.message.MessageService;
import com.forcet.service.rest.util.ServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class SpecieAnimalServiceImpl implements SpecieAnimalService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    private static final String ERROR_NO_EXIST = "No se encontro el elemento buscado";

    @Autowired
    SpecieAnimalRepository specieAnimalRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    MessageService messageService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MaintenanceRepository maintenanceRepository;

    ServiceUtil serviceUtil = new ServiceUtil();

    @Override
    public ResponseEntity<Summary<SpecieAnimalResponse>> saveSpecieAnimal(SpecieAnimalRequest request) {
        Summary<SpecieAnimalResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieAnimalResponse>> responseEntity;
        try {
            SpecieAnimalResponse specieAnimalResponse = specieAnimalRepository.findSpecieAnimalByUser(request.getUserCode(), request.getAnimalId()).orElse(null);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("El animal registrados no existe"),
                    "El animal ingresado ya se encuentra registrado", HttpStatus.CONFLICT.value()), HttpStatus.CONFLICT);
            if (specieAnimalResponse == null ) {
                SpecieAnimalResponse response = specieAnimalRepository.save(getNewSpecieAnimalResponse(request));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieAnimalResponse>> updateSpecieAnimal(SpecieAnimalRequest request) {
        Summary<SpecieAnimalResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieAnimalResponse>> responseEntity;
        try {
            SpecieAnimalResponse specieAnimalResponse = specieAnimalRepository.findSpecieAnimalByUser(request.getUserCode(), request.getAnimalId()).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe ningun animal registrado"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (specieAnimalResponse != null) {
                SpecieAnimalResponse response = specieAnimalRepository.save(getUpdateSpecieAnimalResponse(request, specieAnimalResponse));
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(response), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieAnimalResponse>> deleteSpecieAnimal(String animalId) {
        Summary<SpecieAnimalResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieAnimalResponse>> responseEntity;
        try {
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el animal buscado"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            SpecieAnimalResponse specieAnimalResponse = specieAnimalRepository.findById(animalId).orElse(null);
            if (specieAnimalResponse != null) {
                specieAnimalRepository.delete(specieAnimalResponse);
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieAnimalResponse>> deleteAllSpecieAnimal(String code) {
        Summary<SpecieAnimalResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieAnimalResponse>> responseEntity;
        try {
            List<SpecieAnimalResponse> responses = specieAnimalRepository.findAllSpecieAnimalByUser(code);
            specieAnimalRepository.deleteAll(responses);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieAnimalResponse>> findSpecieAnimalById(String animalId) {
        Summary<SpecieAnimalResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieAnimalResponse>> responseEntity;
        try {
            SpecieAnimalResponse specieAnimalResponse = specieAnimalRepository.findById(animalId).orElse(null);
            responseEntity =  new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList("No existe el animal buscado"),
                    ERROR_NO_EXIST, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
            if (specieAnimalResponse != null) {
                responseEntity = new ResponseEntity<>(summary.createSuccessResponse(specieAnimalResponse), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<SpecieAnimalResponse>> findAllSpecieAnimalByUser(String userId) {
        Summary<SpecieAnimalResponse> summary = new Summary<>();
        ResponseEntity<Summary<SpecieAnimalResponse>> responseEntity;
        try {
            List<SpecieAnimalResponse> product = specieAnimalRepository.findAllSpecieAnimalByUser(userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(product), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Summary<List<String>>> saveSpecieAnimalFile(MultipartFile[] files, String userId) {
        ResponseEntity<Summary<List<String>>> responseEntity;
        Summary<List<String>> summary = new Summary<>();
        try {
            List<String> resultErrors = AnimalFile.create(fileRepository, messageService, userRepository, specieAnimalRepository, maintenanceRepository).saveUserFiles(files, userId);
            responseEntity = new ResponseEntity<>(summary.createSuccessResponse(resultErrors), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            responseEntity = new ResponseEntity<>(summary.createErrorResponse(Collections.singletonList(e.getMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }


    public SpecieAnimalResponse getNewSpecieAnimalResponse(SpecieAnimalRequest request) {
        LocalDate birthDate = serviceUtil.getLocalDate(request.getBirthDate());
        long day = serviceUtil.getDaysCountBetweenDates(birthDate, LocalDate.now());
        long months = serviceUtil.getMonthsCountBetweenDates(birthDate, LocalDate.now());
        long years = serviceUtil.getYearsCountBetweenDates(birthDate, LocalDate.now());
        return SpecieAnimalResponse.builder()
                .animalId(request.getAnimalId())
                .userCode(request.getUserCode())
                .name(request.getName())
                .birthDate(dateFormat(request.getBirthDate()))
                .codeMaintenance(request.getCodeMaintenance())
                .codeMaintenanceDescription(getMaintenanceDescription(request.getUserCode(), request.getCodeMaintenance()))
                .cost(request.getCost())
                .total(request.getTotal())
                .diaryConsume(request.getDiaryConsume())
                .notifyUser(request.getNotifyUser())
                .age(new BigDecimal(day))
                .month(new BigDecimal(months))
                .year(new BigDecimal(years))
                .build();
    }

    private SpecieAnimalResponse getUpdateSpecieAnimalResponse(SpecieAnimalRequest request, SpecieAnimalResponse response) {
        LocalDate birthDate = serviceUtil.getLocalDate(request.getBirthDate());
        long day = serviceUtil.getDaysCountBetweenDates(birthDate, LocalDate.now());
        long months = serviceUtil.getMonthsCountBetweenDates(birthDate, LocalDate.now());
        long years = serviceUtil.getYearsCountBetweenDates(birthDate, LocalDate.now());
        return SpecieAnimalResponse.builder()
                .id(response.getId())
                .animalId(response.getAnimalId())
                .userCode(response.getUserCode())
                .name(request.getName())
                .birthDate(dateFormat(request.getBirthDate()))
                .codeMaintenance(request.getCodeMaintenance())
                .codeMaintenanceDescription(getMaintenanceDescription(response.getUserCode(), request.getCodeMaintenance()))
                .cost(request.getCost())
                .total(request.getTotal())
                .diaryConsume(request.getDiaryConsume())
                .notifyUser(request.getNotifyUser())
                .age(new BigDecimal(day))
                .month(new BigDecimal(months))
                .year(new BigDecimal(years))
                .build();
    }

    private String dateFormat(String date) {
        LocalDate localDate = serviceUtil.getLocalDate(date);
        return serviceUtil.getStringDate(localDate);
    }

    private String getMaintenanceDescription(String code, String codeMaintenance) {
        if (Optional.ofNullable(maintenanceRepository).isPresent()) {
            MaintenanceResponse maintenanceResponse = maintenanceRepository.findMaintenanceByUser(code, codeMaintenance).orElse(null);
            if (maintenanceResponse != null) {
                return maintenanceResponse.getName();
            }
        }
        return null;
    }
}
