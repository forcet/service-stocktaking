package com.forcet.service.rest.security;

import com.forcet.service.rest.error.ExceptionFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private EntryPoint entryPoint;

    @Autowired
    private AuthenticationToken authenticationToken;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        AuthorizationFilter authorizationFilter = new AuthorizationFilter(authenticationToken);
        ExceptionFilter exceptionFilter = new ExceptionFilter(messageSource);

        http.cors()
                .and().
                csrf().
                disable()
                .exceptionHandling().authenticationEntryPoint(entryPoint).and().
                authorizeRequests()
                .antMatchers(HttpMethod.POST, Constants.SIGN_UP_URL).permitAll()
                .antMatchers("/auth", "/users/saveUser",
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources",
                        "/configuration/security",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/swagger-resources/configuration/ui",
                        "/swagger-resources/configuration/security").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(authorizationFilter).addFilterBefore(exceptionFilter, AuthorizationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }
}
