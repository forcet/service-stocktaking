package com.forcet.service.rest.model.fixed.request;


import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FixedRequest {


    @ApiModelProperty(position = 1, notes = "${fixed.description.code}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String fixedId;  //Codigo

    @ApiModelProperty(position = 2, notes = "${fixed.description.userCode}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String userCode; //Codigo de usuario

    @ApiModelProperty(position = 3, notes = "${fixed.description.name}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String name; //Nombre

    @ApiModelProperty(position = 4, notes = "${fixed.description.quantities}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal quantities; //Cantidad

    @ApiModelProperty(position = 5, notes = "${fixed.description.status}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String status; //Estado

    @ApiModelProperty(position = 6, notes = "${fixed.description.cost}", required = true)
    @NotNull(message = "{default.message.notnull}")
    BigDecimal cost; //

    @ApiModelProperty(position = 7, notes = "${fixed.description.quantityLose}")
    BigDecimal quantityLose;

    @ApiModelProperty(position = 8, notes = "${fixed.description.messageLose}")
    List<String> messageLose;

}
