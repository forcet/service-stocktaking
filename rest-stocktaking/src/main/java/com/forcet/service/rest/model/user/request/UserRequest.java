package com.forcet.service.rest.model.user.request;

import com.forcet.service.rest.model.user.Person;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Getter 
@Setter
public class UserRequest {

    @ApiModelProperty(position = 1, notes = "${data.user.nickName}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String username;

    @ApiModelProperty(position = 2, notes = "${data.user.password}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String password;

    @ApiModelProperty(position = 3, notes = "${data.user.status}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String status;

    @ApiModelProperty(position = 4, notes = "${data.user.rol}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    private String rol;

    @ApiModelProperty(position = 5, notes = "${data.user.person}", required = true)
    @NotNull(message = "{default.message.notnull}")
    private Person person;


}
