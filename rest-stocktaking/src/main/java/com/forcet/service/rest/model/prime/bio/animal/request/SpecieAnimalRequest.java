package com.forcet.service.rest.model.prime.bio.animal.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.List;


@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpecieAnimalRequest {

    @ApiModelProperty(position = 1, notes = "${animal.description.code}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String animalId; //Codigo tabla

    @ApiModelProperty(position = 2, notes = "${animal.description.userCode}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String userCode; //Codigo Usuario

    @ApiModelProperty(position = 3, notes = "${animal.description.name}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String name; //Nombre especie

    @ApiModelProperty(position = 4, notes = "${animal.description.birthDate}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String birthDate; //Fecha nacimiento o compra

    @ApiModelProperty(position = 5, notes = "${animal.description.codeMaintenance}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String codeMaintenance; //Codigo del alimento - relacion Mantenanice

    @ApiModelProperty(position = 6, notes = "${animal.description.cost}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal cost; //cuanto costo compralo

    @ApiModelProperty(position = 7, notes = "${animal.description.total}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal total; // unidades si es necesario

    @ApiModelProperty(position = 8, notes = "${animal.description.diaryConsume}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @PositiveOrZero
    BigDecimal diaryConsume; // Consumo de alimentacion a restar en

    @ApiModelProperty(position = 9, notes = "${animal.description.loseSpecie}")
    @PositiveOrZero
    BigDecimal loseSpecie;

    @ApiModelProperty(position = 10, notes = "${animal.description.messageLose}")
    List<String> messageLose;

    @ApiModelProperty(position = 11, notes = "${animal.description.notifyUser}", required = true)
    @NotNull(message = "{default.message.notnull}")
    @NotBlank
    String notifyUser;

}
