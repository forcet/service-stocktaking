package com.forcet.service.rest.service.files;

import com.forcet.service.rest.model.files.File;
import com.forcet.service.rest.model.response.Summary;
import com.forcet.service.rest.repository.files.FileRepository;
import org.springframework.core.io.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class FileServiceImpl implements FileService {

    private static final String ERROR_GENERIC = "Ha ocurrido un error";

    @Autowired
    FileRepository fileRepository;


    @Override
    public Summary<?> save(MultipartFile[] files, String code) {
        Summary<?> summary = new Summary<>();
        try {
            fileRepository.init(code);
            Arrays.asList(files).forEach(file -> fileRepository.save(file, code));
            summary = summary.createSuccessResponse();
        }catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;
    }

    @Override
    public Summary<Resource> load(String name, String code) {
        Summary<Resource> summary = new Summary<>();
        try {
            Resource file = fileRepository.load(name, code);
            summary = summary.createSuccessResponse(file);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;
    }

    @Override
    public Summary<?> deleteAll(String code) {
        Summary<?> summary = new Summary<>();
        try {
            fileRepository.deleteAll(code);
            summary = summary.createSuccessResponse();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;
    }

    @Override
    public Summary<List<File>> loadAll(String code) {
        Summary<List<File>> summary = new Summary<>();
        try (Stream<Path> inputs = fileRepository.loadAll(code).get()) {
            List<File> fileList = inputs.map(path -> {
                String name = path.getFileName().toString();
                return File.builder().name(name).build();
            }).collect(Collectors.toList());
            summary = summary.createSuccessResponse(fileList);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;
    }

    @Override
    public Summary<?> deleteFile(String name, String code) {
        Summary<?> summary = new Summary<>();
        try {
            if (fileRepository.deleteFile(name, code)) {
                summary = summary.createSuccessResponse();
            } else {
                summary = summary.createErrorResponse(Collections.singletonList("No puedo ser borrado"),  "Error al borrar", HttpStatus.EXPECTATION_FAILED.value());
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            summary = summary.createErrorResponse(Collections.singletonList(e.getLocalizedMessage()),
                    ERROR_GENERIC, HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return summary;
    }
}
