package com.forcet.service.rest.model.report.chart;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
public class Data {

    String name;
    BigDecimal value;

}
